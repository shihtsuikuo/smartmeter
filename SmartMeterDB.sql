CREATE TABLE `APP_Description` (
  `appID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `typeID` int(10) unsigned NOT NULL,
  `roomID` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `remark` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `creator` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `create_time` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`appID`),
  KEY `fk_typeID` (`typeID`),
  CONSTRAINT `fk_typeID` FOREIGN KEY (`typeID`) REFERENCES `APP_Type` (`typeID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


CREATE TABLE `APP_EventRecord` (
  `Idx` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `meterID` int(11) NOT NULL,
  `appID` int(10) unsigned NOT NULL,
  `status` int(11) DEFAULT NULL,
  `power` float NOT NULL,
  `power_fac` float DEFAULT NULL,
  `signatureIdx` int(11) DEFAULT NULL,
  PRIMARY KEY (`Idx`,`timestamp`,`appID`),
  KEY `fp_appID` (`appID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


CREATE TABLE `APP_EventRecord30` (
  `Idx` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `meterID` int(11) NOT NULL,
  `appID` int(10) unsigned NOT NULL,
  `status` int(11) DEFAULT NULL,
  `power` float NOT NULL,
  `power_fac` float DEFAULT NULL,
  `signatureIdx` int(11) DEFAULT NULL,
  PRIMARY KEY (`Idx`,`timestamp`,`appID`),
  KEY `fp_appID` (`appID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `APP_Type` (
  `typeID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type_eng_name` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type_ch_name` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `remark` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `State` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `III_code` int(11) DEFAULT NULL,
  `creator` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `create_time` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`typeID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `Energy` (
  `Idx` int(11) NOT NULL AUTO_INCREMENT,
  `meterID` int(11) NOT NULL,
  `timestamp` datetime NOT NULL,
  `phaseID` int(11) DEFAULT NULL,
  `voltage` float DEFAULT NULL,
  `current` float DEFAULT NULL,
  `pwr_factor` float DEFAULT NULL,
  `Hz` float DEFAULT NULL,
  `active_pwr` float DEFAULT NULL,
  `reactive_pwr` float DEFAULT NULL,
  `kw_hr` double DEFAULT NULL,
  `var_hr` double DEFAULT NULL,
  `va_hr` double DEFAULT NULL,
  `apparence_pwr` double DEFAULT NULL,
  `fae` double DEFAULT NULL,
  PRIMARY KEY (`Idx`),
  KEY `fk_power_meter1` (`meterID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

CREATE TABLE `meter` (
  `meterID` int(11) NOT NULL,
  `roomID` varchar(20) NOT NULL,
  `registerTime` date DEFAULT NULL,
  `ip` varchar(45) DEFAULT NULL,
  `model` varchar(45) DEFAULT NULL,
  `phaseID` int(11) DEFAULT NULL,
  `firmwareVersion` int(11) DEFAULT NULL,
  `parentMeterID` int(11) DEFAULT NULL,
  `status` varchar(1) DEFAULT NULL,
  `live` varchar(1) DEFAULT NULL,
  `remark` varchar(45) DEFAULT NULL,
  `creator` varchar(20) NOT NULL,
  `EndTimestamp` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `StartTimestamp` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`meterID`),
  KEY `parent_meter` (`meterID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `Signature` (
  `Idx` int(11) NOT NULL AUTO_INCREMENT,
  `signID` int(11) NOT NULL,
  `meterID` int(11) NOT NULL,
  `timestamp` timestamp NULL DEFAULT NULL,
  `sanpshot` int(11) DEFAULT NULL,
  `phaseID` int(11) DEFAULT NULL,
  `vrms` float DEFAULT NULL,
  `irms` float DEFAULT NULL,
  `avgP` float DEFAULT NULL,
  `avgQ` float DEFAULT NULL,
  `maxP` float DEFAULT NULL,
  `maxQ` float DEFAULT NULL,
  `P` float DEFAULT NULL,
  `Q` float DEFAULT NULL,
  `har0` float DEFAULT NULL,
  `har1` float DEFAULT NULL,
  `har2` float DEFAULT NULL,
  `har3` float DEFAULT NULL,
  `har4` float DEFAULT NULL,
  `har5` float DEFAULT NULL,
  `har6` float DEFAULT NULL,
  `har7` float DEFAULT NULL,
  `har8` float DEFAULT NULL,
  `har9` float DEFAULT NULL,
  PRIMARY KEY (`Idx`,`signID`),
  KEY `meterID` (`meterID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

