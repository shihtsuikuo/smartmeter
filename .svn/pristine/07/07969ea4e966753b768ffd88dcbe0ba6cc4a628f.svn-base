/**
 * @author : Leo, Kuo
 * @date : 2012/5/20
 * @vision :
 * @E-Mail : sky12999@gmail.com
 * @Goal : 取得Meter 之Socket 之連線
 * @Package 長度: [0]:head ,[1]:length,[2-5]:timestamp,[6]:Phase
 */

package com;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Properties;

import javax.net.SocketFactory;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import bean.ApplianceInfo;
import bean.EnergyInfo;

import com.driver.EngeryReportConfig_REQ;
import com.driver.Protocol;
import com.driver.AppReportConfig_REQ;
import com.driver.RebootMeterConfig_REQ;

import tool.DBConnection;
import tool.DateUtil;

public class SocketThread extends Thread {

	/**
	 * Meter IP_Address
	 */
	private String address = "";
	/**
	 * Meter port Default :5566
	 */
	static int port = 5566;
	/**
	 * Meter timeout
	 */
	static int timeout = 1000; // ms
	/**
	 * polling time , 多久要polling 一次
	 */
	static int pollingTime = 1000; // ms
	/**
	 * Meter Id uniqure
	 */
	private int meterId = 0;
	/**
	 * 設定收集Energy information 的time Interval
	 */
	private static int timeInterval = 5;// s
	/**
	 * 開啟JDBC 連線for Mysql
	 */
	public Connection conn = null;
	public Socket clientSocket = null;
	public int rebootSignal = 0; //1 for reboot, 0 do nothing
	/**
	 * cassandra key space config
	 */
	final static String keyspace = "Power";
	/**
	 * cassandra key column family
	 */
	final static String colFName = "Meter";
	private Logger LogFile = Logger.getLogger(SocketThread.class);
	private int recordCount = 0;
	static Properties props;
	static {
		props = new Properties();
		
		try {
			props.load(new FileInputStream("meter.properties"));
		} catch (FileNotFoundException e) {
			System.out.println("Can't find file.");
		} catch (IOException e) {
			System.out.println(e.toString());
		}
		timeout = Integer.parseInt(props.getProperty("timeout"));
		pollingTime = Integer.parseInt(props.getProperty("pollingTime"));
		timeInterval = Integer.parseInt(props.getProperty("timeInterval"));
	}

	/**
	 * 
	 * @param conn
	 * @param address
	 * @param port
	 * @param meterId
	 */
	public SocketThread(String address, int port, int meterId) {
		super(address);
		this.address = address;
		this.port = port;
		this.meterId = meterId;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public int getMeterId() {
		return meterId;
	}

	public void setMeterId(int meterId) {
		this.meterId = meterId;
	}

	@Override
	/**
	 * 
	 */
	public void run() {
		//PropertyConfigurator.configure("log4j.properties");
		//LogFile.setLevel(Level.INFO);
		System.out.println("DEBUG >>"+LogFile.isDebugEnabled());
		System.out.println("INFO >>"+LogFile.isInfoEnabled());
		System.out.println("TRACE >>"+LogFile.isTraceEnabled());
		System.out.println("WARN >>"+LogFile.isEnabledFor(Level.WARN));
		System.out.println("DEBUG >>"+LogFile.isEnabledFor(Level.DEBUG));
		System.out.println(""+LogFile.getName());
		System.out.println(""+LogFile.getPriority());
		BufferedOutputStream output = null;
		BufferedInputStream input = null;
		AppReportConfig_REQ appREQ = null;
		EngeryReportConfig_REQ engerREQ = null;
		RebootMeterConfig_REQ rebootREQ = null;
		LogFile.info("--------開始" + address + "連線\t--------");
		try {
			// 設定Socket time out 的時間
			clientSocket = SocketFactory.getDefault().createSocket();
			SocketAddress remoteaddr = new InetSocketAddress(address, port);
			clientSocket.connect(remoteaddr, timeout);

			LogFile.info(address + " is connected ?"
					+ clientSocket.isConnected());

			output = new BufferedOutputStream(clientSocket.getOutputStream());
			input = new BufferedInputStream(clientSocket.getInputStream());

			engerREQ = new EngeryReportConfig_REQ(output);

			engerREQ.setPhase(engerREQ.PHASE_A);
			engerREQ.setTimeInterval(timeInterval);
			engerREQ.start();
			appREQ = new AppReportConfig_REQ(output); // 建立 Appliance Report
														// Config
														// Requent
			appREQ.start();
			output.flush();
			byte[] cmdd ={(byte) 0x78};
			
			Thread.sleep(1000);
			LogFile.info("System connect Meter :" + address);
			LogFile.info(address + " setPhase :" + engerREQ.getPhase());
			LogFile.info(address + " setTimeInterval :"
					+ engerREQ.getTimeInterval());
			// energy回傳
			EnergyInfo energyInfo = new EnergyInfo();
			ApplianceInfo appInfo = new ApplianceInfo();

			conn = DBConnection.getConnection();

			// LogFile.info("開始JDBC 連線:" + conn.getClientInfo());
			int countEnergy = 0;

			int applianceCount = 0;

			while (clientSocket.isConnected()) { // 不斷讀取。
								
				if (input.available() > 0 && input.available() > 5) {
					byte[] ch = new byte[input.available()];

					byte command = 0;
					int resLength = 0;
					// LogFile.println(input.available());
					// 用，查看是Socket 是否有回傳資料
					input.read(ch, 0, 5);// 檢查head由 ch第5個字元開始讀取 讀取五個位元數，會先回傅一個//
											// ch[0] :0x78, ch[1,2,3,4]
					// System.out.println(input.available()+"<=>"+rd);
					
					if (ch[0] == Protocol.HEAD) {
						resLength = Protocol.getIntLength(ch, 1);// package 取得長度
						if (resLength > 0) {
							byte[] resPackage = new byte[resLength];

							input.read(resPackage, 0, resLength); // 把inputStream
																	// 放到
																	// respackage
							LogFile.warn(Arrays.toString(resPackage));
							command = resPackage[0]; // check commond head
							if (command == 0x02) {
								System.out.print(address
										+ "ReportConfig_RSP>> ");
								if (resPackage[2] == 0x01) {
									System.out.println("Success!");
								} else if (resPackage[2] == 0x02) {
									System.out.println("Access Deny!");
								} else if (resPackage[2] == 0x03) {
									System.out.println("set Failed!");
								}
							} else if (command == 0x04) {// meter_power

								energyInfo = energyInfo.getEnergy(resPackage);// 取得EnergyInfo

								System.out.print("["
										+ energyInfo.getTimestamp()
										+ "] Got Energy_info "
										+ (countEnergy++) + "\t[V]:"
										+ energyInfo.getV() + "\t[I]:"
										+ energyInfo.getA() + "\t[W]:"
										+ energyInfo.getW() + "\t[Meter]:"
										+ address);
								try {
									/* 20131019 disable insert Energy for the time being*/
									//System.out.println("No inserting Energy this time");
									insertEnergy(conn, energyInfo, meterId);// 塞入
																			// ENERGY
								} catch (Exception e) {
									// TODO: handle exception
									System.out.println("insertEnergy error !!!");
								}

							} else if (command == 0x05) { // meter appliance
															// Event
								appInfo = appInfo.getAppInfo(resPackage);
								System.out.println("[" + appInfo.getTimestamp()
										+ "]Get Application "
										+ (applianceCount++) + "\t[appID]:"
										+ appInfo.getAppID() + "\t[Power]:"
										+ appInfo.getPower() + "\t[State]:"
										+ appInfo.getAppState() + "\t[SigIdx]:"
										+ appInfo.getSignatureIndex()
										+ "\t[Meter]:" + address);
								try {
									insertAppliance(conn, appInfo, meterId);// 塞入
																			// APPLIANCE
								} catch (Exception e) {
									// TODO: handle exception
									System.out
											.println("insertAppliance error!!!!");
								}

								// insertAppliance(conn2, appInfo, meterId);

							} else if (command == 0x0f) {
								System.out.println("MeterConfigSet_RSP>> ");
								if (resPackage[2] == 0x01) {
									System.out.println("Success!");
								} else if (resPackage[2] == 0x02) {
									System.out.println("Access Deny!");
								} else if (resPackage[2] == 0x03) {
									System.out.println("set Faild!");
								}
							}
						} else if (input.available() < 0) {
							System.out.print("[" + DateUtil.showTime() + "]"
									+ "Meter socket error ");
							break;
						}
					}
					/* check the rebootSignal first */
					if(rebootSignal == 1){
						rebootREQ = new RebootMeterConfig_REQ(output);
						rebootREQ.start();
						output.flush();
						if (input.available() > 0 && input.available() > 5){
							System.out.println("GET Reboot Command Response.");
						}
						rebootSignal = 0;
					}

				} else {
					Thread.sleep(pollingTime);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("Thread STOP !!");
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			System.out.println("要結束了~!!!");
			try {
				appREQ.stop();
				engerREQ.stop();
				clientSocket.close();
				output.close();
				input.close();
				if (!conn.isClosed() && conn != null) {
					conn.close();
				}
				/*
				 * if(!conn2.isClosed()){ conn2.close(); }
				 */
				LogFile.info("------------------/資料庫連線關閉/----------------");

			} catch (SQLException e1) {
				e1.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}
	
	public void setRebootSignal(int n){
			rebootSignal = n;
	}

	/**
	 * 將Appliance的資料存到Mysql SmartMeterDB.app_rec
	 * 
	 * @param cn
	 * @param info_in
	 * @param meterID
	 * @throws Exception
	 */
	public void insertAppliance(Connection cn, ApplianceInfo appInfo,
			int meterID) throws Exception {
		PreparedStatement stmt = null;
		// Connection xconn=null;
		// if(cn.isClosed() || cn==null)
		// {
		// cn=DBConnection.getConnection();
		//
		// }

		try {
			// xconn=DBConnection.getConnection();
			if (cn.isClosed() || cn == null) {
				cn = DBConnection.getConnection();

			}
			stmt = cn
					.prepareStatement("INSERT INTO APP_EventRecord(`timestamp`,`meterID`, `appID`, `status`, `power`, `power_fac`, `signatureIdx`) "
							+ "VALUES (?,?,?,?,?,?,?)");
			
			stmt.setString(1, appInfo.getTimestamp());
			stmt.setInt(2, meterID);
			stmt.setInt(3, appInfo.getAppID());
			stmt.setInt(4, appInfo.getAppState());
			stmt.setFloat(5, appInfo.getPower());
			stmt.setFloat(6, appInfo.getFactor());
			stmt.setInt(7, appInfo.getSignatureIndex());
			stmt.executeUpdate();
			LogFile.debug(stmt.toString());

		} catch (Exception e) {
			LogFile.error(e.toString());
		} finally {
			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException e) {
				LogFile.error(e.toString());
			}
			// xconn.close();

		}
	}

	/**
	 * 把資料加入table:power
	 * 
	 * @param cn
	 * @param info_in
	 * @param meterID
	 * @throws Exception
	 */
	public void insertEnergy(Connection cn, EnergyInfo info, int meterID) {

		// Connection xconn=null;
		PreparedStatement stmt = null;
		try {
			// stmt.setQueryTimeout(60); // if can't inset 60s
			if (cn.isClosed() || cn == null) {
				cn = DBConnection.getConnection();

			}
			
			stmt = cn
					.prepareStatement("INSERT INTO Energy(`meterID`, `timestamp`, `phaseID`, `voltage`, `current`, `pwr_factor`, `Hz`, `active_pwr`, `reactive_pwr`, `apparence_pwr`, `kw_hr`, `var_hr`, `va_hr`,`fae`) "
							+ "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
			stmt.setInt(1, meterID);
			stmt.setString(2, info.getTimestamp());
			stmt.setInt(3, info.getPahaseId());
			stmt.setFloat(4, info.getV());
			stmt.setFloat(5, info.getA());// current
			stmt.setFloat(6, info.getFactor());// pwr_factor
			stmt.setFloat(7, info.getHz());// HZ
			stmt.setFloat(8, info.getW());// active_pwr
			stmt.setFloat(9, info.getVAR());// reactive_pwr
			stmt.setFloat(10, info.getVA());// apparence_pwr
			stmt.setFloat(11, info.getWh() / 1000);// kw_hr
			stmt.setFloat(12, info.getVARh());// var_hr
			stmt.setFloat(13, info.getVAh());// va_hr
			stmt.setFloat(14, 0);// fae
			LogFile.debug(stmt.toString());
			stmt.executeUpdate();

		} catch (Exception e) {
			LogFile.error(e.toString());
		} finally {
			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException e) {
				LogFile.error(e.toString());
			}

		}

	}

}
