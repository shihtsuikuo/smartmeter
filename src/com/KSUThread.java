package com;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.log4j.*;

import bean.ApplianceInfo;

import com.driver.Protocol;

import tool.DBConnection;
import tool.DateUtil;

public class KSUThread extends Thread {//30秒的thread
	private static int pollingTime = 5000;
	private static Logger log = Logger.getLogger(KSUThread.class);//log 紀錄

	public KSUThread() {
		super("KSUTHREAD");
		//PropertyConfigurator.configure("KSUT.properties");
	}

	public static void main(String[] args) {
		log.debug("1");
	}

	public void run() {//start

		log.setLevel(Level.WARN);
		log.debug("2");
		System.out.println("KSUTHREAD START");
		Statement statement1 = null;

		ResultSet rs = null;
		ResultSet rs2 = null;
		Connection DBconn1 = null;

		int AppID[] = new int[100];
		long now=0;
		int index = 0;
		boolean initFlag = true;
		now = System.currentTimeMillis();
		//System.out.println("small test");
		//System.out.println("time is "+now);
		System.out.println("test time >>"+Protocol.longToTimestamString(System.currentTimeMillis()));
		
		try {
			DBconn1 = DBConnection.getConnection();
			

			// DBconn2 = DBConnection.getConnection2();
			// statement2 = DBconn2.createStatement();
			statement1 = DBconn1.createStatement();
			String queryAppID = "select appID from APP_Description";//取得database 裡面有多少appID要做
			rs = statement1.executeQuery(queryAppID);

			while (rs.next()) {
				AppID[index] = rs.getInt("appID");
				// MeterID[index] = rs.getInt("meterID");
				index++;//計算有多少APPID 要去塞
			}
			rs.close();
			statement1.close();
			
			if(DBconn1.isClosed())
			{
				
				
			}
			else {
				DBconn1.close();
				DBconn1=null;
			}
			System.out.println("initFlag is go >>");
			while (true) 
			{
				try {
					
				
					
					if(DBconn1==null || DBconn1.isClosed())//連線database
					{
						DBconn1 = DBConnection.getConnection();
					}
	
					PropertyConfigurator.configure("KSUT.properties");
					log.setLevel(Level.WARN);
					log.debug("----->Starting polling Applinace<------ " + index);
					// System.out.println();
					Thread.sleep(pollingTime);
					statement1 = DBconn1.createStatement();
					now = System.currentTimeMillis();
					long before = now - 2*pollingTime;
					for (int i = 0; i < index; i++) {
						// System.out.println("Query event!!"+AppID[index]);
						String subQuery = "select * from APP_EventRecord where 1=1" 
								+ " and appID ="
								+ AppID[i]
								+ " and timestamp > '"
								+ Protocol.longToTimestamString(before)
								+ "'"
								+ " and timestamp <= '"
								+ Protocol.longToTimestamString(now)
								+ "'"
								+ " order by timestamp desc limit 1";
						log.debug(subQuery);
						rs = statement1.executeQuery(subQuery);//取得目前有沒有資料
						// System.out.println(AppID[i] + "get query size ="+
						// rs.getFetchSize());
						
						int meterID = 0;
						
						if (rs.next())  //如果有前一個狀態的話
						{  
							//沒有近來
							// insert table
							// System.out.println("init Insert! Appliance zero! >>>>>>>>3");
							System.out.println("[" + DateUtil.showTime()
									+ "] init Insert! get Appliance event !>>>>>>>>3");
							ApplianceInfo app = new ApplianceInfo(rs, AppID[i],
									meterID);
							// insertAppliance(DBconn1, app, meterID);
							
							//System.out.println(">>>> rs.get :"+rs.getFloat("power"));
							
							System.out.println(">>>> power is :"+app.getPower());
							insertAppliance(DBconn1, app, meterID);
						}
						else
						{
							// insert Zero
							if (initFlag) 
							{//剛開機的狀態
								
								System.out.println("init Insert! Appliance zero! >>>>>>>>1");
								ApplianceInfo app = new ApplianceInfo(AppID[i],
										meterID, 0);
								// insertAppliance(DBconn1, app, meterID);
								insertAppliance(DBconn1, app, meterID);
								if (i == index - 1) 
								{
									initFlag = false;
									System.out.println("initFlag is false !!!!!!!!!");
								}
							} 
							else
							{//之後的狀態
								//System.out.println("KSUT debug3 ---should be here");
								//看不懂~!!
								// System.out.println("Insert! Appliance zero! >>>>>>>>>>0");
	//							String insStr = "INSERT INTO APP_EventRecord30(`meterID`, `appID`, `status`, `power`, `power_fac`, `signatureIdx`) "
	//									+ "select meterID,"
	//									+ AppID[i]
	//									+ ",status,power,power_fac,signatureIdx from APP_EventRecord30 where 1=1 "
	//									+ "and appID ="
	//									+ AppID[i]
	//									+ "  order by timestamp desc limit 1";
								
								//有錯?
								///String insStr = "INSERT INTO APP_EventRecord30(`meterID`, `appID`, `status`, `power`, `power_fac`, `signatureIdx`,`timestamp`) "
								/*String insStr = "INSERT INTO NILM(`meterID`, `appID`, `status`, `power`, `power_fac`, `signatureIdx`,`timestamp`) "
										+ "select meterID,appID"
										+ ",status,power,power_fac,signatureIdx "
										+", '"+Protocol.longToTimestamString(System.currentTimeMillis())+"' as timestamp" // 以目前
										//+" from APP_EventRecord30 where 1=1 "
										+" from NILM where 1=1 "
										+ "and appID ="
										+ AppID[i]
										+ "  order by Idx desc limit 1";
								*/
								
								
								subQuery = "select meterID,appID,status,power,power_fac,signatureIdx from APP_EventRecord30 where 1=1 and " +
								//subQuery = "select meterID,appID,status,power,power_fac from NILM where 1=1 and " +
										"appID =" + AppID[i]+
										"  order by timestamp desc limit 1";
								rs2 = statement1.executeQuery(subQuery);
								
								if(rs2.next())
								{
								
									ApplianceInfo app = new ApplianceInfo(rs2, AppID[i],
											meterID);
									if( AppID[i]==1)
									{
										System.out.println(">>>>>>new xpower :"+app.getPower()); 
									}
									insertAppliance(DBconn1, app, meterID);
									
								}
								rs2.close();
								
								
								
								if( AppID[i]==1)
								{
									//System.out.println(insStr);
								}
								//statement1.executeUpdate(insStr);
							}
							
						}
	
					}
					statement1.close();
					// statement2.close();
					rs.close();
					
					} 
					catch (Exception e) {
						// TODO: handle exception
						System.out.println("small DBconn1 error");
						log.error(Protocol.longToTimestamString(System.currentTimeMillis())+" >>  small DBconn1 error");
					}
				}//while
			
			
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				if (DBconn1 != null) {
					DBconn1.close();
				}
				// DBconn2.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	/**
	 * 將Appliance的資料存到Mysql SmartMeterDB.app_rec
	 * 
	 * @param cn
	 * @param info_in
	 * @param meterID
	 * @throws Exception
	 */
	public static void insertAppliance(Connection cn, ApplianceInfo appInfo,
			int meterID) throws Exception {
		PreparedStatement stmt = null;
		try {
			stmt = cn
				.prepareStatement("INSERT INTO APP_EventRecord30(`meterID`, `appID`, `status`, `power`, `power_fac`, `signatureIdx`,`timestamp`) "
						+ "VALUES (?,?,?,?,?,?,?)");		
			//stmt = cn
			//		.prepareStatement("INSERT INTO NILM(`meterID`, `appID`, `status`, `power`, `power_fac`,`timestamp`) "
			//				+ "VALUES (?,?,?,?,?,?)");		
			stmt.setInt(1, meterID);
			stmt.setInt(2, appInfo.getAppID());
			stmt.setInt(3, appInfo.getAppState());
			stmt.setFloat(4, appInfo.getPower());
			stmt.setFloat(5, appInfo.getFactor());
			stmt.setInt(6, appInfo.getSignatureIndex());
			stmt.setString(7,Protocol.longToTimestamString(System.currentTimeMillis()));
			
			if(appInfo.getAppID()==1)
			{
				//System.out.println(stmt.toString());
			}
			//log.debug(stmt.toString());
			stmt.executeUpdate();

		} catch (Exception e) {
			log.error(e.toString());
		} finally {
			try {
				if (stmt != null) {
					stmt.close();
				} 
			} catch (SQLException e) {
				log.error(e.toString());
			}

		}
	}
}
