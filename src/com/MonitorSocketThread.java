package com;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;

public class MonitorSocketThread extends Thread{

	static Properties props;
	static {
		props = new Properties();
		
		try {
			props.load(new FileInputStream("meter.properties"));
		} catch (FileNotFoundException e) {
			System.out.println("Can't find file.");
		} catch (IOException e) {
			System.out.println(e.toString());
		}
		
		
	}
	ArrayList<SocketThread> sockets = new ArrayList<SocketThread>();
//	ArrayList<RebootThread> timers = new ArrayList<RebootThread>();
	/**
	 * time單位 :ms
	 */
	int pollingTime =  12 * 60 * 1000; //12 hr
	public MonitorSocketThread(){
		
	}
	@Override
	public void run(){
		this.setPollingTime(Integer.parseInt(props.getProperty("rebootPollingTime")));
		System.out.println("reboot polling time: " + pollingTime);
		System.out.println("Meter Array List Size: "+sockets.size());
		while(true){
			try {
				Thread.sleep(this.pollingTime);
				//while(sockets.size() <= 0) System.out.println("No Connection");
				for (SocketThread cThread : sockets){
					System.out.println(cThread.getName());
					System.out.println(cThread.getId());
					String address = cThread.getAddress();
					int port = cThread.getPort();
					int meterID = cThread.getMeterId();
					/*System.out.println("Meter ID "+meterID +"-->"+cThread.isAlive() );
					if(!cThread.isAlive()){

						cThread = new SocketThread(address,port,meterID);
						cThread.start(); 
					}*/
					System.out.println("===============Regular Reboot Meter#" + cThread.getMeterId() + "==================");
					cThread.setRebootSignal(1);
					Thread.sleep(10000); //wait for 10 sec and start it again.
					if(!cThread.isAlive()){
					//if(true){	
						remove(cThread);
						System.out.println("RESTART CONNECTION: address<"+address+">");
						SocketThread newsocket = new SocketThread(address, port, meterID);
						this.add(newsocket);
						//System.out.println("Meter Array List Size: "+sockets.size());
					}
				}
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * 把要Monitor 的Thread 加入
	 * @param e
	 */
	public void add(SocketThread e){
		//System.out.println("Meter added into monitoring list: "+e.getName());
		//System.out.println(e.getAddress());
		this.sockets.add(e);
		e.start();
	}
	
	public void remove(SocketThread e){
		long id = e.getId();
		this.sockets.remove(e);
		//System.out.println("Remove Meter#" + id);
		
	}
	/**
	 * 設定多少微秒要檢查childThread 的State
	 * @param time
	 */
	public void setPollingTime(int time){
		this.pollingTime = time;
	}
	
	
	
	
}
