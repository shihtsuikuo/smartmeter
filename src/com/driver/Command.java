package com.driver;

public interface Command {
	public void stop();
	public void start();
}
