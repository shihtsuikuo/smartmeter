/**
 * @author : KC,KUO
 * @date : 2012/9/15
 * @vision :1.0
 * @E-Mail : sky12999@gmail.com
 * @Goal :設定Meter report config 收集 appliance event
 */
package com.driver;


import java.io.BufferedOutputStream;
import java.io.IOException;


public class AppReportConfig_REQ extends Config implements Command{


	private BufferedOutputStream output = null;
	/**
	 * interface
	 */
	public static final byte ETHERNET = 0x01;
	/**
	 * interface
	 */
	public static final byte XBee = 0x02;
	/**
	 * commond
	 */
	public static final byte CMD = 0x02;
	private byte interfaceType = 0;

	public AppReportConfig_REQ(BufferedOutputStream output) {
		this.output = output;
		setInterface(ETHERNET);
	}

	/**
	 * Meter 開始收集資料
	 * 
	 * @return
	 */
	public void start() {
		byte CRC = 0;
		byte[] cmd = { Protocol.HEAD,// head
				0x00,// length
				0x00, // length
				0x00,// length
				0x0a, // length
				0x01, // ReportConfig_REQ:0x00,01
				0x00, //
				interfaceType,// Ethernet
				0x01,// Appliance information.
				0x02, // evnet
				0x00, // Time interval should report default 0x00,00,00,01
				0x00, //
				0x00,//
				0x00, //
				CRC // CRC 由系統自動計算
		};// start
		cmd = Protocol.getCommondLength(cmd);
		cmd = Protocol.getCMD(cmd);
		// for (int i = 0; i < cmd.length; i++)
		// System.out.println("cmd[" + i + "]=" + (cmd[i] & 0xFF));
		try {
			output.write(cmd);
			output.flush();
		//	this.result(input, (byte) 0x02); // 回傳 commond 結果
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	/**
	 * 停止Meter收集資料
	 * 
	 * @return
	 */
	public void stop() {
		byte CRC = 0;
		byte[] cmd = { Protocol.HEAD,// head
				0x00,// length
				0x00,// length
				0x00,// length
				0x0a,// length
				0x01,// ReportConfig_REQ:0x00,01
				0x00,// ReportConfig_REQ
				0x01,// interface
				0x01,// report data
				0x00,// report mode =00 :stop
				0x00,// time
				0x00,// time
				0x00,// time
				0x00,// time
				CRC //
		};// stop
		cmd = Protocol.getCommondLength(cmd);
		cmd = Protocol.getCMD(cmd);
		try {
			output.write(cmd);
			output.flush();
		//	this.result(input, (byte) 0x02); // 回傳 commond 結果
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	/**
	 * 設定 interfacetype,
	 * 
	 * @param type
	 *            ETHERNET,XBee
	 */
	public void setInterface(byte type) {
		interfaceType = type;
	}
}
