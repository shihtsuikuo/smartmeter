/**
 * @author : KC,KUO
 * @date : 2012/9/15
 * @vision :1.0
 * @E-Mail : sky12999@gmail.com
 * @Goal :設定Meter report config 收集  energy information
 */
package com.driver;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;


public class EngeryReportConfig_REQ extends Config implements Command {

	private BufferedOutputStream output = null;
	/**
	 * interface
	 */
	public static final byte ETHERNET = 0x01;
	/**
	 * interface
	 */
	public static final byte XBee = 0x02;
	/**
	 * command
	 */
	public static final byte CMD = 0x02;
	public static final byte PHASE_A = 0x01;
	public static final byte PHASE_B = 0x02;
	public static final byte PHASE_C = 0x04;
	public static final byte PHASE_N = 0x10;
	/**
	 * Phase A+B+C
	 */
	public static final byte PHASE_ALL = 0x07;
	/**
	 * Phase A+B+C+N
	 */
	public static final byte PHASE_ALL_N = 0x17;
	/**
	 * defult value : 0x07 = PHASE_ALL
	 */
	private byte phase = 0x07;
	private byte interfaceType = 0;
	/**
	 * 設定how 收long Meter 的Energy 資料
	 */
	public int timeInterval = 1;

	public EngeryReportConfig_REQ(BufferedOutputStream output) {
		this.output = output;
		setInterface(ETHERNET);
	}

	/**
	 * Meter 開始收集資料
	 * 
	 * @return
	 */
	public void start() {
		byte CRC = 0;
		byte[] cmd = { Protocol.HEAD,// head
				0x00,// length
				0x00, // length
				0x00,// length
				0x0a, // length
				0x01, // ReportConfig_REQ:0x00,01
				0x00, //
				interfaceType,// Ethernet
				0x02,// Appliance information.
				0x01, // evnet
				0x00, // Time interval should report default 0x00,00,00,01
				0x00, //
				0x00,//
				0x00, //
				CRC // CRC 由系統自動計算
		};// start
		cmd = Protocol.getCommondLength(cmd);
		cmd = getTimeIntervalByte(cmd, this.timeInterval);
		cmd = Protocol.getCMD(cmd);
		
		byte[] cmdPhase = getEnergyPhase_REQ();
 		// for (int i = 0; i < cmd.length; i++)
		// System.out.println("cmd[" + i + "]=" + (cmd[i] & 0xFF));
		try {
			output.write(cmd);
			output.flush();
			output.write(cmdPhase);
			output.flush();
			// this.result(input, (byte) 0x02); // 回傳 commond 結果
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * 停止Meter收集資料
	 * 
	 * @return
	 */
	public void stop() {
		byte CRC = 0;
		byte[] cmd = { Protocol.HEAD,// head
				0x00,// length
				0x00,// length
				0x00,// length
				0x0a,// length
				0x01,// ReportConfig_REQ:0x00,01
				0x00,// ReportConfig_REQ
				0x01,// interface
				0x01,// report data
				0x00,// report mode =00 :stop
				0x00,// time
				0x00,// time
				0x00,// time
				0x00,// time
				CRC //
		};// stop
		cmd = Protocol.getCommondLength(cmd);
		cmd = Protocol.getCMD(cmd);

		try {
			output.write(cmd);
			output.flush();
			// this.result(input, (byte) 0x02); // 回傳 commond 結果
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * 設定 interfacetype,
	 * 
	 * @param type
	 *            ETHERNET,XBee
	 */
	public void setInterface(byte type) {
		interfaceType = type;
	}

	/**
	 * 設定多少秒回報一次
	 * 
	 * @param data
	 *            輸入字串
	 * @param interval
	 *            單位: second
	 * @return
	 */
	public static byte[] getTimeIntervalByte(byte[] data, int interval) {
		byte[] time = Protocol.intToByteArray(interval);
		time = Protocol.inverseByte(time);
		return Protocol.offsetCopy(data, time, 10);
	}

	/**
	 * 設定Energy report 的Phase A,B,C
	 * 
	 * @return
	 */
	private byte[] getEnergyPhase_REQ() {
		byte CRC = 0;
		byte[] energyRepoert_REQ = { Protocol.HEAD, // head
				0x00, // length 0
				0x00, // length 1
				0x00, // length 2
				0x04, // length 3
				0x03, // commod LSB
				0x00, // commod MSB
				phase, // 0x01:Phase A,0x02:Phase B,0x04:Phase,0x10:Phase N
						// current,0x07 All Phase,0x17 All phase +phase N
						// current
				CRC // CRC 由系統自動計算
		};
		energyRepoert_REQ = Protocol.getCommondLength(energyRepoert_REQ);
		energyRepoert_REQ = Protocol.getCMD(energyRepoert_REQ);
		return energyRepoert_REQ;
	}

	public int getTimeInterval() {
		return timeInterval;
	}

	public void setTimeInterval(int timeInterval) {
		this.timeInterval = timeInterval;
	}

	public byte getPhase() {
		return phase;
	}

	/**
	 * 設定Energy report 的Phase A,B,C
	 * 
	 * @param phase
	 */
	public void setPhase(byte phase) {
		this.phase = phase;
	}

}
