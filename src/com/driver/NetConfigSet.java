package com.driver;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.net.InetAddress;
public class NetConfigSet extends Config {
	public static void main(String[] args) {
		//new netConfigSet("192.168.1.101", true);
	
	}

	BufferedOutputStream output = null;
	BufferedInputStream input = null;
	Socket clientSocket;
	/**
	 * 設定Meter 之 IP address, 
	 * @param IPadd  , 系統預設192.168.1.30
	 * @param maskIP ,如果輸入 NULL, 使用系統預設: 255.255.255.0
	 * @param gatewayIP, 如果輪入NULL,使用系統預設: 192.168.1.1
	 * @param enableDHCP , true :使用static ip, false :使用DHCP
	 */
	public NetConfigSet() {


	}
	/**
	 * 
	 * 設定Meter 之 IP address, 
	 * @param IPadd  , 系統預設192.168.1.30
	 * @param maskIP ,如果輸入 NULL, 使用系統預設: 255.255.255.0
	 * @param gatewayIP, 如果輪入NULL,使用系統預設: 192.168.1.1
	 * @param enableDHCP , true :使用static ip, false :使用DHCP
	 */
	public void setMeterNet(String IPadd,String maskIP,String gatewayIP, boolean enableDHCP){
		loadProperties();
		String configIP = props.getProperty("meter_default_ip");
		int configPort = Integer.parseInt(props
				.getProperty("meter_default_port"));
		String configGateway = props.getProperty("meter_default_gateway");
		String configMask = props.getProperty("meter_default_mask");
		if(IPadd !=null){
			configIP = IPadd;
		}
		if(gatewayIP != null ){
			configGateway = gatewayIP;
		}
		if(maskIP != null){
			configMask = maskIP;
		}
		try {
			clientSocket = new Socket(configIP, configPort);
			clientSocket.setSoTimeout(10000); // if can't find connect
			System.out.println("IS connected ?" + clientSocket.isConnected());
			output = new BufferedOutputStream(clientSocket.getOutputStream());
			input = new BufferedInputStream(clientSocket.getInputStream());
			byte[] ip =InetAddress.getByName(IPadd).getAddress();

			byte[] gateway =InetAddress.getByName(configGateway).getAddress();
			byte[] mark =InetAddress.getByName(configMask).getAddress();
			byte[] cmd = new byte[21];
			byte CRC = 0;
			byte length = 0;
			cmd[0] = 0x78;
			cmd[1] = 0x00;
			cmd[2] = 0x00;
			cmd[3] = 0x00;
			cmd[4] = 0x10; // length 16
			cmd[5] = 0x0A; // NetConfigSet
			cmd[6] = 0x00; // NetConfigSet
			if (enableDHCP) {
				cmd[7] = 0x00; // 0x00: Static IP , 0x01: DHCP
			} else {
				cmd[7] = 0x01;
			}
			// IP
			cmd = Protocol.offsetCopy(cmd, ip, 8);
			cmd = Protocol.offsetCopy(cmd, gateway, 12);
			cmd = Protocol.offsetCopy(cmd, mark, 16);
			cmd[20] = CRC;
			cmd = Protocol.getCommondLength(cmd);
			cmd = Protocol.getCMD(cmd);
			for (int i = 0; i < cmd.length; i++)
				System.out.println("cmd[" + i + "]=" + (cmd[i] & 0xFF));
			output.write(cmd);
			output.flush();
			this.result(input,(byte) 0x0b);
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("Can't find Host");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally{
			try {
				clientSocket.close();
				output.close();
				input.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}
}
