package com.driver;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.Socket;
import java.util.Properties;

public class Config {
	
	final private int protocolSize = 30;
	
	String[] protocol = new String[protocolSize];
	public static Properties props;

	/**
	 * 讀取properties
	 */
	public static void loadProperties() {
		props = new Properties();
		try {
			props.load(new FileInputStream("meter.properties"));
		} catch (FileNotFoundException e) {
			System.out.println("Can't find file.");
		} catch (IOException e) {
			System.out.println(e.toString());
		}
	}

	public void set() {
	}

	public BufferedInputStream result(BufferedInputStream input, byte cmd) {

		try {
			loadProperties();
			getProtocolMessage();
			
			String message = protocol[(cmd & 0xff)]; // 由meter.properties取得設定檔
			System.out.println(message);
			Thread.sleep(1000);// 系統300ms 處理一次package
			byte[] ch = new byte[256];
			System.out.println(input.available());
			input.read(ch, 0, 5); // 檢查head由 ch第5個字元開始讀取 讀取五個位元數，會先回傅一個
			// ch[0] :0x78, ch[1,2,3,4] :packeagge
			// length
			if (ch[0] == Protocol.HEAD) {

				int resLength = Protocol.getIntLength(ch, 1);// package 取得長度
				if (resLength > 0) {
					byte[] resPackage = new byte[resLength];
					input.read(resPackage, 0, resLength); // 把inputStream 放到
															// respackage
					byte commond = resPackage[0]; // check commond head
					if (commond == cmd) {
						if (resPackage[2] == 0x01) {
							System.out.println(message + "\tSuccess! ");
						} else if (resPackage[2] == 0x02) {
							System.out.println(message + "\tAccess Deny!");
						} else if (resPackage[2] == 0x03) {
							System.out.println(message + "\tset Faild!");
						} else if (resPackage[2] == 0x04) {
							System.out.println(message + "\tInvalid address");
						}
					}

				}
			}
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return input;
	}

	/**
	 *取得 protocol Message 
	 */
	public void getProtocolMessage() {
		loadProperties();
		int i=0;
		do{
			i++;
			protocol[i] = props.getProperty("protocol_" + i);
//			System.out.println(protocol[i]);
		}while(protocol[i] !=null);
	}
}
