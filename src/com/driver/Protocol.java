/**
 * @author : Leo Kuo
 * @date : 2012/5/20
 * @vision :
 * @E-Mail : sky12999@gmail.com
 * @Goal : 建立Meter 之 SmartME Protocal V0.2.1 
 * @Goal : 建立Meter 之 JASON Protocal
 */

package com.driver;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

public class Protocol {

	/**
	 * 
	 */
	static byte CRC = 0;
	/**
	 * 中原標時間
	 */
	static long timeZone = 0;
	static boolean timeEnable=false;
//	public static byte[] ENERGYREPORT_REQ = { 0x78, // head
//			0x00, // length 0
//			0x00, // length 1
//			0x00, // length 2
//			0x04, // length 3
//			0x03, // commod LSB
//			0x00, // commod MSB
//			0x07, // 0x01:Phase A,0x02:Phase B,0x04:Phase,0x10:Phase N
//					// current,0x07 All Phase,0x17 All phase +phase N current
//			CRC // CRC 由系統自動計算
//	};
	/**
	 * energy -start
	 */
//	public static byte[] REPORTCONFIG_REQ_CMD_ENERGY = { 0x78, // head
//			0x00, // length
//			0x00, // length
//			0x00, // length
//			0x0a, // length
//			0x01, // ReportConfig_REQ:0x00,01
//			0x00, //
//			0x01, // Ethernet
//			0x02, // Energy information
//			0x01, // Report Mode 0x00:No report, 0x01 periodical,0x02 event,0x03
//					// pull,0x04 Event&pull
//			0x01, // Time interval should report default 0x00,00,00,01
//			0x00, //
//			0x00, //
//			0x00, //
//			CRC // CRC由系統自動計算
//	};

	// final static byte[]
	// REPORTCONFIG_REQ_CMD={0x78,0x00,0x00,0x00,0x0a,0x01,0x00,0x01,0x02,0x03,0x05,0x00,0x00,0x00,0x04};//start
//	public static byte[] STOP_CMD = { 0x78, // head
//			0x00, // length
//			0x00, // length
//			0x00, // length
//			0x0a, // length
//			0x01, // cmd low 0001 ReportConfig_REQ:0x0001
//			0x00, // cmd hignt
//			0x01, // Interface Type : 01:Ether
//			0x02, // Report Data Type:
//			0x00, 0x00, 0x00, 0x00, 0x00, CRC };// stop
//
//	// appliance
//	public static byte[] REPORTCONFIG_REQ_CMD_APPLIANCE = { 0x78,// head
//			0x00,// length
//			0x00, // length
//			0x00,// length
//			0x0a, // length
//			0x01, // ReportConfig_REQ:0x00,01
//			0x00, //
//			0x01,// Ethernet
//			0x01,// Appliance information.
//			0x02, // evnet
//			0x00, // Time interval should report default 0x00,00,00,01
//			0x00, //
//			0x00,//
//			0x00, //
//			CRC // CRC 由系統自動計算
//	};// start
//	public static byte[] cmd3 = { 0x78, 0x00, 0x00, 0x00, 0x0a, 0x01, 0x00,
//			0x01, 0x01, 0x02, 0x01, 0x00, 0x00, 0x00, 0x02 };// start
//	public static byte[] cmd4 = { 0x78, 0x00, 0x00, 0x00, 0x0a, 0x01, 0x00,
//			0x01, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01 };// stop
	/**
	 * packageHead 0x78 = 120
	 */
	public static byte HEAD = 0x78;
	// MeterConfigSet_REQ
	static byte timestamp1 = 0;
	static byte timestamp2 = 0;
	static byte timestamp3 = 0;
	static byte timestamp4 = 0;
	final static byte[] MeterConfigSet_REQ = { 0x78, // head 0
			0x00, // length 1
			0x00, // length 2
			0x00, // length 3
			0x11, // length 4
			0x0e, // CMD L 5
			0x00, // CMD H 6
			0x00, // ID 0
			0x00, // ID 1
			0x00, // ID 2
			0x00, // ID 3
			0x00, // ID 4
			0x00, // ID 5
			0x00, // ID 6
			0x00, // ID 7
			0x00, // timestamp LSD0 15
			0x00, // timestamp LSD1 16
			0x00, // timestamp LSD2 17
			0x00, // timestamp MSD3 18
			0x00,// time zone default :0
			0x01,// type
			CRC };// stop
	static Properties props;
	public static void main(String[] args) {
		Date date = new Date();
		// long timestemp = date.getTime() / 1000 - 28800;
		long timestemp = date.getTime() ; //get from system time
		DateFormat dataFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); // 存到Data
		// base
		// 之timeStamp
		long time = System.currentTimeMillis();
		System.out.println(dataFormat.format(date));
		System.out.println(dataFormat.format(timestemp));
		System.out.println(dataFormat.format(time));
	}
	// read meter.properties
	static {
		props = new Properties();
		try {
			props.load(new FileInputStream("meter.properties"));
			timeZone = Integer.parseInt(props.getProperty("timeZone"));
			timeEnable = Boolean.parseBoolean(props.getProperty("timeEnable"));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			System.out.println("Can't find file.");
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println(e.toString());
			e.printStackTrace();
		}
		
		
	}
	/**
	 * 4 byte array to int Length
	 * 
	 * @param b
	 * @param index
	 * @return
	 */
	public static int getIntLength(byte[] b, int index) {
		int l;
		l = b[index + 3];
		l &= 0xff;
		l |= ((long) b[index + 2] << 8);
		l &= 0xffff;
		l |= ((long) b[index + 1] << 16);
		l &= 0xffffff;
		l |= ((long) b[index] << 24);
		return l;
	}

	/**
	 * 將Byte 轉成 Integer
	 * 
	 * @param b
	 * @param index
	 * @return
	 */
	public static int getint(byte[] b, int index) {
		int l;
		l = b[index];
		l &= 0xff;
		l |= ((long) b[index + 1] << 8);
		l &= 0xffff;
		l |= ((long) b[index + 2] << 16);
		l &= 0xffffff;
		l |= ((long) b[index + 3] << 24);
		return l;
	}

	/**
	 * 將Byte 轉成 Long
	 * 
	 * @param b
	 * @param index
	 * @return
	 */
	public static long getLong(byte[] b, int index) {
		long l;
		l = b[index];
		l &= 0xff;
		l |= ((long) b[index + 1] << 8);
		l &= 0xffff;
		l |= ((long) b[index + 2] << 16);
		l &= 0xffffff;
		l |= ((long) b[index + 3] << 24);
		return l;
	}

	/**
	 * 將Byte 轉成 Float
	 * 
	 * @param b
	 * @param index
	 * @return
	 */
	public static float getFloat_1(byte[] b, int index) {
		int l;
		l = b[index + 0];
		l &= 0xff;
		l |= ((long) b[index + 1] << 8);
		l &= 0xffff;
		l |= ((long) b[index + 2] << 16);
		l &= 0xffffff;
		l |= ((long) b[index + 3] << 24);
		return Float.intBitsToFloat(l);
	}

	/**
	 * 輸入CMD，自動做check sum
	 * 
	 * @param data
	 * @return
	 */
	public static byte[] getCMD(byte[] data) {
		byte res = 0;
		for (int i = 5; i < data.length - 1; i++) {
			res ^= data[i]; // 做XOR
		}
		// System.out.println(res);
		data[data.length - 1] = res;

		return data;
	}
	/**
	 * 輸入Data & check sun 
	 * @param data
	 * @return
	 */
	public static byte getCRC(byte[] data){
		byte res = 0;
		for (int i = 0; i < data.length-1; i++) {
			res ^= data[i];
		}
		return res;
	}
	// 取得Commnd 的長度
	public static byte[] getCommondLength(byte[] data) {
		int length = data.length - 5;
		//System.out.println("length =" + length);
		byte[] array = intToByteArray(length);
		data[1] = array[0];
		data[2] = array[1];
		data[3] = array[2];
		data[4] = array[3];
		return data;
	}

	/**
	 * 
	 * @param b
	 * @param index
	 * @param end
	 * @return
	 */
	public static float decodeFloat(byte[] b, int index, int end) {
		float res = 0;
		String temp = "";
		for (int i = end; i >= index; i--) {
			// temp += Integer.parseInt(Byte.toString(b[i]);
		}

		res = Float.parseFloat(temp);
		return res;
	}

	/**
	 * 將4 Byte array ，轉成 IEEE 754 single float
	 * 
	 * @param buf
	 * @param pos
	 * @return
	 */
	public static float getFloat(byte[] buf, int pos) {
		int bits = 0;
		int i = 0;

		for (int shifter = 0; shifter <= 3; shifter++) {
			bits |= ((int) buf[i + pos] & 0xff) << (shifter * 8);
			i++;
		}

		return Float.intBitsToFloat(bits);
	}

	/**
	 * 將Long 轉成 Byte Array
	 * 
	 * @param i
	 * @return
	 */
	public static byte[] longToByteArray1(long i) {
		byte[] result = new byte[4];
		result[0] = (byte) ((i >> 24) & 0xFF); // LSB
		result[1] = (byte) ((i >> 16) & 0xFF);
		result[2] = (byte) ((i >> 8) & 0xFF);
		result[3] = (byte) (i & 0xFF);// MSB
		return result;
	}

	/**
	 * 自動取得System timestamp , 用來教正Meter Timestamp
	 * 
	 * @return
	 */
	public static byte[] getMeterConfigSet_REQ() {

		Date date = new Date();
		// long timestemp = date.getTime() / 1000 - 28800;
		long timestemp = date.getTime() / 1000;

		MeterConfigSet_REQ[18] = (byte) ((timestemp >> 24) & 0xFF); // LSB
		MeterConfigSet_REQ[17] = (byte) ((timestemp >> 16) & 0xFF);
		MeterConfigSet_REQ[16] = (byte) ((timestemp >> 8) & 0xFF);
		MeterConfigSet_REQ[15] = (byte) (timestemp & 0xFF); // MSB
		byte[] temp = getCMD(MeterConfigSet_REQ);
		// for (int i = 0; i < temp.length; i++) {
		// System.out.println("[" + i + "] = " + temp[i]);
		// }
		byte[] temptime = { MeterConfigSet_REQ[15], MeterConfigSet_REQ[16],
				MeterConfigSet_REQ[17], MeterConfigSet_REQ[18] };
		DateFormat mediumFormat = DateFormat.getDateTimeInstance(
				DateFormat.MEDIUM, DateFormat.MEDIUM);
		System.out.println("SET Time >>"
				+ mediumFormat.format(Protocol.getLong(temptime, 0) * 1000));
		return temp;
	}

	/**
	 * 作 integer 轉成 hex
	 * 
	 * @param num
	 *            int 輸入要轉成hex ,
	 * @param size
	 *            output size
	 * @param fixByte
	 *            在前面補0 or f
	 * @return
	 */
	public static String intToHexString(int num, int size, String fixByte) {
		String output = "";
		int length = Integer.toHexString(num).length();
		for (int j = 1; j <= size - length; j++) {
			output += fixByte;
		}
		output += Integer.toHexString(num);
		return output;
	}

	/**
	 * 將Integer 轉成 4Byte Array
	 * 
	 * @param value
	 * @return
	 */
	public static byte[] intToByteArray(int value) {
		byte[] b = new byte[4];
		for (int i = 0; i < 4; i++) {
			int offset = (b.length - 1 - i) * 8;
			b[i] = (byte) ((value >>> offset) & 0xFF);
		}
		return b;

	}
	/**
	 * 4 byte array copy
	 * @param data 原史字串
	 * @param array 要COPY字串的
	 * @param offset 原史字串要位移的距離
	 */
	public static byte[] offsetCopy(byte[] data, byte[] array, int offset) {
		data[offset] = array[0];
		data[offset + 1] = array[1];
		data[offset + 2] = array[2];
		data[offset + 3] = array[3];
		return data;

	}

	/**
	 * 取得 日期格式 : yyyy/MM/dd hh:MM:ss
	 * 
	 * @param data
	 * @return
	 */
	public static String byteToTimestampString(byte[] data) {
		Date date = new Date();
		// long timestemp = date.getTime() / 1000 - 28800;
		long timestemp = System.currentTimeMillis(); //get from system time
	    // true : get timestamp from meter
		// false: get timestamp from gateway
		if(timeEnable){
			timestemp = (Protocol.getLong(data, 0)+timeZone) * 1000; // 單位millisecond
		}
		DateFormat dataFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); // 存到Data
		// base
		// 之timeStamp

		// LogFile.println("Timestamp = "+timestamp
		// +"Now >>"+date.getTime()+"["+dataFormat .format(date.getTime())+"]");

		return dataFormat.format(timestemp);
	}

	/**
	 * 取得 日期long :從1970/01/01 的電腦時間開始計算
	 * 
	 * @param data
	 * @return
	 */
	public static long byteToTimestampLong(byte[] data) {
		long time =0;
		if(timeEnable){
			time = (Protocol.getLong(data, 0)) * 1000;
		}else{
			time=System.currentTimeMillis()/1000;
		}
		return  time;// 單位millisecond
	}
	/**
	 * 反轉byte 由MSB or LSB
	 * @param data
	 * @return
	 */
	public static byte[] inverseByte(byte[] data){
		int length = data.length;
		byte[] temp = new byte[length];
		for (int i = 0; i < data.length; i++) {
			temp[i] = data[length-i-1];
		}
		return temp;
	}
	/**
	 * type "yyyy-MM-dd HH:mm:ss"
	 * @param time
	 * @return
	 */
	public static String longToTimestamString(long time){
		DateFormat dataFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); // 存到Data
		// base
		// 之timeStamp
		
		return dataFormat.format(time);
	}

}
