/**
 * @author : Administrator
 * @date : 2012/5/21
 * @vision :
 * @E-Mail : sky12999@gmail.com
 * @Goal :
 */

package com;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.PropertyConfigurator;

import tool.DBConnection;

public class Start {
	/*static String[] meterips;
	static Properties props;
	static {
		props = new Properties();
		
		try {
			props.load(new FileInputStream("meterList.properties"));
		} catch (FileNotFoundException e) {
			System.out.println("Can't find file.");
		} catch (IOException e) {
			System.out.println(e.toString());
		}
		
	}*/
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		/**
		 * log4j 設定檔案
		 */
		// PropertyConfigurator.configure("log4j.properties");
		// new Meterclient();
		// MeterDeamon thread = new MeterDeamon("192.168.1.30", 23);
		// thread.start();
		// new Start();
		// new Meterclient("192.168.1.30");
		
		// 開始
		try{
		if (args.length > 0) {
			for (int i = 0; i < args.length; i++) {
				if (args[i].equals("db")) {
					System.out.println("Datebase Test !!");
					DBConnection.test();
				} else if (args[i].equals("log")) {

				} else if (args[i].equals("start")) {
					System.out.println("Start Connection to Meter !!");
					new Meterclient();
				/*} else if (args[i].equals("meterList")) {
					int meter_number = Integer.valueOf(args[i+1]);
					meterips = new String[meter_number];
					for(int j = 0; j< meter_number; j++){
						meterips[j] = props.getProperty("meter"+Integer.toString(j+1));
						new Meterclient(meterips[j]);
					}
					break;
					//new Meterclient("192.168.0.30");
					//new Meterclient("192.168.0.31");
					//new Meterclient("192.168.0.32");
				
				 */
				} else if (args[i].indexOf(".") > 0) {
					new Meterclient(args[i]);
				}
			}
		} else {
			System.out.println("==================connect meters!================");
			new Meterclient();
		}
		}catch(Exception e){
			System.out.println(e);
		}
		return;
		// new Meterclient("192.168.0.16");
		// new Meterclient("192.168.1.30");
	}

}
