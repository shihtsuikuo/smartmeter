package com;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.util.Properties;
import java.util.Scanner;

import javax.net.SocketFactory;

import org.apache.log4j.*;

public class MeterDeamon extends Thread implements KeyListener {

	String ip;
	int port;
	static Properties props;
	public static Logger log;

	static {
		PropertyConfigurator.configure("meter.properties");
		log = Logger.getLogger(MeterDeamon.class);
		log.setLevel(Level.ALL);

	}

	public static void main(String[] args) {
		MeterDeamon thread = new MeterDeamon(args[1], 23);
		System.out.println("Meter :"+args[1]+"Port :23");
		thread.start();
		// String outputp = "P";
		// System.out.println("P".matches("[1-9]"));
		// System.out.println("1".matches("[1-9]"));
		// if (outputp.indexOf("Detected") >0) {
		//
		// System.out.println("YES");
		//
		// }

	}

	public MeterDeamon(String ip, int port) {
		this.ip = ip;
		this.port = port;

	}

	public void run() {

		BufferedOutputStream output = null;
		BufferedInputStream input = null;
		FileReader fileReader = null;
		BufferedReader buf = null;
		Socket socket = null;

		try {
			socket = SocketFactory.getDefault().createSocket();
			SocketAddress remoteaddr = new InetSocketAddress(ip, port);
			socket.connect(remoteaddr, 30);
			output = new BufferedOutputStream(socket.getOutputStream());
			input = new BufferedInputStream(socket.getInputStream());
			// fileReader = new FileReader(envPATH + host.getPath());
			// buf = new BufferedReader(fileReader);
			// out.print(socket.isConnected());
			System.out.println("socket.isConnected()=" + socket.isConnected());
			int index = 0;
			while (socket.isConnected()) {

				// System.out.println(input.available());
				if (System.in.available() > 0) {
					Scanner scanner = new Scanner(System.in);
					if (scanner.next().endsWith("q")) {
						break;
					}
				}
				if (input.available() > 0) {
					byte[] data = new byte[input.available()];
					input.read(data, 0, input.available());
					String outString = new String(data);
					// outString = outString.replace(outString, "\n");
					// System.out.print(index+">>"+outString);
					if (outString.indexOf("Detected") > 0) {
						String temp = outString.substring(
								outString.indexOf("cluster") + 8,
								outString.indexOf("cluster") + 9);
						// System.out.println("State :"+state);

						int state = -1;
						if (temp.matches("[1-9]")) {
							state = Integer.parseInt(temp);
						} else {
							log.warn(outString);
						}
						String message = new String();
						
						if (outString.indexOf("16777220") > 0) { // light
							// System.out.println("燈泡");
							if (state == 3) {
								state = 1; // on
							} else {
								state = 0; // off
							}
							message = index + "," + "燈泡" + "," + state;
							log.info(message);
							index++;
						}
						if (outString.indexOf("536870917") > 0) { // fan
							// System.out.println("吹風機");
							if (state == 1) {
								state = 1; // on
							} else {
								state = 0; // off
							}
							message = index + "," + "吹風機" + "," + state;
							log.info(message);
							index++;
						}
						if (outString.indexOf("603979779") > 0) { // fan
							// System.out.println("烤箱");
							if (state == 7 || state == 13) {
								state = 1; // on
							} else {
								state = 0; // off
							}
							message = index + "," + "烤箱" + "," + state;
							log.info(message);
							index++;
						}
						if (outString.indexOf("620756994") > 0) { // fan
							// System.out.println("電鍋");
							if (state == 9 || state == 11) {
								state = 1; // on
							} else {
								state = 0; // off
							}
							message = index + "," + "電鍋" + "," + state;
							log.info(message);
							index++;
						}
					}
				}
				Thread.sleep(100);

			}
			output.write("reboot()\r\n".getBytes());
			output.flush();
			socket.close();
			output.close();
			input.close();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				socket.close();
				output.close();
				input.close();
				Runtime.getRuntime().freeMemory();
				log.info("------> Meter Deamon END <------");

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub

	}
}
