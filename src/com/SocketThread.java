/**
 * @author : Leo, Kuo
 * @date : 2012/5/20
 * @vision :
 * @E-Mail : sky12999@gmail.com
 * @Goal : 取得Meter 之Socket 之連線
 * @Package 長度: [0]:head ,[1]:length,[2-5]:timestamp,[6]:Phase
 */

package com;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Properties;

import javax.net.SocketFactory;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import bean.ApplianceInfo;
import bean.EnergyInfo;
import bean.SignatureInfo;

import com.driver.EngeryReportConfig_REQ;
import com.driver.Protocol;
import com.driver.AppReportConfig_REQ;
import com.driver.RebootMeterConfig_REQ;
import com.driver.SignatureReportConfig_REQ;

import tool.DBConnection;
import tool.DateUtil;

public class SocketThread extends Thread {

	/**
	 * Meter IP_Address
	 */
	private String address = "";
	/**
	 * Meter port Default :5566
	 */
	static int port = 5566;
	/**
	 * Meter timeout
	 */
	static int timeout = 1000; // ms
	/**
	 * polling time , 多久要polling 一次
	 */
	static int pollingTime = 1000; // ms
	/**
	 * Meter Id unique
	 */
	private int meterId = 0;
	/**
	 * 設定收集Energy information 的time Interval
	 */
	private static int timeInterval = 1;// s, default value, real value are read from property file.
	/**
	 * 1 > print debug message, 0 > do not print debug message.
	 */
	private static int printDebug = 1;
	private static String[] insertTimeRecord = new String[3];
	private static int phaseSetting = 1; // default:PHASE_A
	private boolean periodically_insert = false;
	private int count_15m = 0;
	float energy_last_15m = 0.0F;
	float energy_last_1hr = 0.0F;
	float energy_last_1day = 0.0F;
	float energy_last_1month = 0.0F;
	float energy_15m = 0.0F;
	float energy_1hr = 0.0F;
	float energy_1day = 0.0F;
	float energy_1month = 0.0F;
	float power_15m = 0.0F;
	/**
	 * 開啟JDBC 連線for Mysql
	 */
	public Connection conn = null;
	public Socket clientSocket = null;
	public int forNCKU = 1;
	public int rebootSignal = 0; //1 for reboot, 0 do nothing
	/**
	 * cassandra key space config
	 */
	final static String keyspace = "Power";
	/**
	 * cassandra key column family
	 */
	final static String colFName = "Meter";
	private Logger LogFile = Logger.getLogger(SocketThread.class);
	static Properties props;
	static {
		props = new Properties();
		
		try {
			props.load(new FileInputStream("meter.properties"));
		} catch (FileNotFoundException e) {
			System.out.println("Can't find file.");
		} catch (IOException e) {
			System.out.println(e.toString());
		}
		timeout = Integer.parseInt(props.getProperty("timeout"));
		pollingTime = Integer.parseInt(props.getProperty("pollingTime"));
		timeInterval = Integer.parseInt(props.getProperty("timeInterval"));
		printDebug = Integer.parseInt(props.getProperty("printDebug"));
		phaseSetting = Integer.parseInt(props.getProperty("phaseSetting"));
	}

	/**
	 * 
	 * @param conn
	 * @param address
	 * @param port
	 * @param meterId
	 */
	public SocketThread(String address, int port, int meterId) {
		super(address);
		this.address = address;
		this.port = port;
		this.meterId = meterId;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public int getMeterId() {
		return meterId;
	}

	public void setMeterId(int meterId) {
		this.meterId = meterId;
	}

	@Override
	/**
	 * 
	 */
	public void run() {
		PropertyConfigurator.configure("log4j.properties");
		LogFile.setLevel(Level.INFO);
		System.out.println("DEBUG >>"+LogFile.isDebugEnabled());
		System.out.println("INFO >>"+LogFile.isInfoEnabled());
		System.out.println("TRACE >>"+LogFile.isTraceEnabled());
		System.out.println("WARN >>"+LogFile.isEnabledFor(Level.WARN));
		System.out.println("DEBUG >>"+LogFile.isEnabledFor(Level.DEBUG));
		//System.out.println(""+LogFile.getName());
		//System.out.println(""+LogFile.getPriority());
		BufferedOutputStream output = null;
		BufferedInputStream input = null;
		AppReportConfig_REQ appREQ = null;
		SignatureReportConfig_REQ sigREQ= null;
		EngeryReportConfig_REQ engerREQ = null;
		RebootMeterConfig_REQ rebootREQ = null;
		insertTimeRecord[0] = "";
		insertTimeRecord[1] = "";
		insertTimeRecord[2] = "";
		
		LogFile.info("--------開始" + address + "連線\t--------");
		try {
			// 設定Socket time out 的時間
			clientSocket = SocketFactory.getDefault().createSocket();
			SocketAddress remoteaddr = new InetSocketAddress(address, port);
			clientSocket.connect(remoteaddr, timeout);

			LogFile.info(address + " is connected ?"
					+ clientSocket.isConnected());

			output = new BufferedOutputStream(clientSocket.getOutputStream());
			input = new BufferedInputStream(clientSocket.getInputStream());

			engerREQ = new EngeryReportConfig_REQ(output);

			//engerREQ.setPhase(engerREQ.PHASE_ALL);
			engerREQ.setPhase(num2byte(phaseSetting));
			engerREQ.setTimeInterval(1);
			engerREQ.start();
			/* Not collecting appliance report */
			appREQ = new AppReportConfig_REQ(output); // 建立 Appliance Report
														// Config
														// Requent
			
			appREQ.start();
			
			sigREQ = new SignatureReportConfig_REQ(output); // 建立 Appliance Report
			sigREQ.start();
			output.flush();
			
			byte[] cmdd ={(byte) 0x78};
			
			Thread.sleep(1000);
			LogFile.info("System connect Meter :" + address);
			LogFile.info(address + " setPhase :" + engerREQ.getPhase());
			LogFile.info(address + " setTimeInterval :"
					+ engerREQ.getTimeInterval());
			// energy回傳
			EnergyInfo energyInfo = new EnergyInfo();
			ApplianceInfo appInfo = new ApplianceInfo();
			SignatureInfo sigInfo = new SignatureInfo();

			conn = DBConnection.getConnection();

			//LogFile.info("開始JDBC 連線:" + conn.getClientInfo());
			int applianceCount = 0;
			
			
			while (clientSocket.isConnected()) { // 不斷讀取。
								
				if (input.available() > 0 && input.available() > 5) {
					byte[] ch = new byte[input.available()];

					byte command = 0;
					int resLength = 0;
					// LogFile.println(input.available());
					// 用，查看是Socket 是否有回傳資料
					input.read(ch, 0, 5);// 檢查head由 ch第5個字元開始讀取 讀取五個位元數，會先回傅一個 // ch[0] :0x78, ch[1,2,3,4] 
											
					if (ch[0] == Protocol.HEAD) {
						resLength = Protocol.getIntLength(ch, 1);// package 取得長度
						if (resLength > 0) {
							byte[] resPackage = new byte[resLength];

							input.read(resPackage, 0, resLength); // 把 inputStream 放到respackage
							command = resPackage[0]; // check command head
							if (command == 0x02) {
								SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
						    	System.out.print( sdf.format(Calendar.getInstance().getTime()) );
								System.out.print(address + " ReportConfig_RSP>> ");
								if (resPackage[2] == 0x01) {
									System.out.println("Success!");
								} else if (resPackage[2] == 0x02) {
									System.out.println("Access Deny!");
								} else if (resPackage[2] == 0x03) {
									System.out.println("set Failed!");
								}
							} else if (command == 0x04) {// meter_power

								energyInfo = energyInfo.getEnergy(resPackage);// 取得EnergyInfo
								if(printDebug == 1)
									System.out.println("["	+ energyInfo.getTimestamp()	+ "] SocketThread Got Energy_info ");
										//+ (countEnergy++) + "\t[V]:"
										//+ energyInfo.getV() + "\t[I]:"
										//+ energyInfo.getA() + "\t[W]:"
										//+ energyInfo.getW() + "\t[Meter]:"
										//+ address);
								try {
									if(energyInfo.getTimestamp().charAt(18) != '0'){ 
										if(engerREQ.getTimeInterval() > 1){
											power_15m += energyInfo.getW()*timeInterval;
											count_15m += timeInterval;
											engerREQ.setTimeInterval(1);
											engerREQ.start();
										}
										if(printDebug == 1)
										System.out.println("aligning timestamp.......");
										
										if(insertTimeRecord[energyInfo.PahaseId-1].length() == 0)
											continue;
										else if(getSubtractSecond(energyInfo.getTimestamp(), insertTimeRecord[energyInfo.PahaseId-1]) < timeInterval)
											continue;
									}else if(engerREQ.getTimeInterval() < timeInterval){
										engerREQ.setTimeInterval(timeInterval);
										engerREQ.start();
									}
									insertTimeRecord[energyInfo.PahaseId-1] = new String(energyInfo.getTimestamp());
									insertEnergy(conn, energyInfo, meterId);// 塞入
																			// ENERGY
									if(energy_last_15m == 0) energy_last_15m = energyInfo.getWh();
									if(energy_last_1hr == 0) energy_last_1hr = energyInfo.getWh();
									if(energy_last_1day == 0) energy_last_1day = energyInfo.getWh();
									
									if(!periodically_insert) continue;
									
									power_15m += energyInfo.getW()*timeInterval;
									count_15m += timeInterval;
									if(checkTimestamp(15,rounded(energyInfo.getTimestamp())) == true){
										energy_15m = energyInfo.getWh() - energy_last_15m;
										if(printDebug == 1) System.out.println("Insert 15m accumulate data, Wh:" + energyInfo.getWh() + ", last 15m:" + energy_last_15m + ", in 15m:" + energy_15m);
										insertEnergy_15m(conn,energyInfo, meterId, energy_15m/1000, power_15m/count_15m);
										count_15m = 0;
										power_15m = 0.0F;
										energy_last_15m = energyInfo.getWh();
										
									}
									
									if(checkTimestamp(60,rounded(energyInfo.getTimestamp())) == true){
										if(printDebug == 1) System.out.println("Insert 1hr accumulate data.");
										energy_1hr = energyInfo.getWh() - energy_last_1hr;
										insertEnergy_1hr(conn,energyInfo, meterId, energy_1hr/1000);
										energy_last_1hr = energyInfo.getWh();
									}
									if(checkTimestamp(1440,rounded(energyInfo.getTimestamp())) == true){
										if(printDebug == 1) System.out.println("Insert 1-day accumulate data.");
										energy_1day = energyInfo.getWh() - energy_last_1day;
										insertEnergy_period(1,conn,energyInfo, meterId, energy_1day/1000);
										energy_last_1day = energyInfo.getWh();
									}
									if(/*count_1month >= accumulate_1hr*24*30 || */checkTimestamp(43200,rounded(energyInfo.getTimestamp())) == true){
										if(printDebug == 1) System.out.println("Insert 1month accumulate data.");
										insertEnergy_period(2,conn,energyInfo, meterId, energy_1month/1000);
										energy_1month = 0.0F;
									}
									
								} catch (Exception e) {

									System.out.println(e.toString());
									System.out.println("insertEnergy error !!!");
								}

							} else if (command == 0x05) { // meter appliance Event
								appInfo = appInfo.getAppInfo(resPackage);
								System.out.println("[" + appInfo.getTimestamp()
										+ "]Get Application "
										+ (applianceCount++) + " [appID]:"
										+ appInfo.getAppID() + " [Power]:"
										+ appInfo.getPower() + " [State]:"
										+ appInfo.getAppState() + " [SigIdx]:"
										+ appInfo.getSignatureIndex()
										+ " [Meter]:" + address);
								LogFile.info("[" + appInfo.getTimestamp()
										+ "]Get Application "
										+ (applianceCount++) + " [appID]:"
										+ appInfo.getAppID() + " [Power]:"
										+ appInfo.getPower() + " [State]:"
										+ appInfo.getAppState() + " [SigIdx]:"
										+ appInfo.getSignatureIndex()
										+ " [Meter]:" + address);
								try {
									insertAppliance(conn, appInfo, meterId);// 塞入
								} catch (Exception e) {
									System.out.println("insertAppliance error!!!!");
								}

								// insertAppliance(conn2, appInfo, meterId);

							} else if (command == 0x06) {// signature

								sigInfo = sigInfo.getSig(resPackage);// 取得SignatureInfo


								try {
									
									insertSignature(conn, sigInfo, meterId);// insert signature
									/*										
									if(energy_last_15m == 0) energy_last_15m = energyInfo.getWh();
									if(energy_last_1hr == 0) energy_last_1hr = energyInfo.getWh();
									if(energy_last_1day == 0) energy_last_1day = energyInfo.getWh();
									
									power_15m += energyInfo.getW()*timeInterval;
									count_15m += timeInterval;
									if(checkTimestamp(15,rounded(energyInfo.getTimestamp())) == true){
										energy_15m = energyInfo.getWh() - energy_last_15m;
										if(printDebug == 1) System.out.println("Insert 15m accumulate data, Wh:" + energyInfo.getWh() + ", last 15m:" + energy_last_15m + ", in 15m:" + energy_15m);
										insertEnergy_15m(conn,energyInfo, meterId, energy_15m/1000, power_15m/count_15m);
										count_15m = 0;
										power_15m = 0.0F;
										energy_last_15m = energyInfo.getWh();
										
									}
									
									if(checkTimestamp(60,rounded(energyInfo.getTimestamp())) == true){
										if(printDebug == 1) System.out.println("Insert 1hr accumulate data.");
										energy_1hr = energyInfo.getWh() - energy_last_1hr;
										insertEnergy_1hr(conn,energyInfo, meterId, energy_1hr/1000);
										energy_last_1hr = energyInfo.getWh();
									}
									if(checkTimestamp(1440,rounded(energyInfo.getTimestamp())) == true){
										if(printDebug == 1) System.out.println("Insert 1-day accumulate data.");
										energy_1day = energyInfo.getWh() - energy_last_1day;
										insertEnergy_period(1,conn,energyInfo, meterId, energy_1day/1000);
										energy_last_1day = energyInfo.getWh();
									}
									if(checkTimestamp(43200,rounded(energyInfo.getTimestamp())) == true){
										if(printDebug == 1) System.out.println("Insert 1month accumulate data.");
										insertEnergy_period(2,conn,energyInfo, meterId, energy_1month/1000);
										energy_1month = 0.0F;
									}*/
									
								} catch (Exception e) {

									System.out.println(e.toString());
									System.out.println("insertEnergy error !!!");
								}

							} else if (command == 0x0f) {
								System.out.println("MeterConfigSet_RSP>> ");
								if (resPackage[2] == 0x01) {
									System.out.println("Success!");
								} else if (resPackage[2] == 0x02) {
									System.out.println("Access Deny!");
								} else if (resPackage[2] == 0x03) {
									System.out.println("set Faild!");
								}
							}
						} else if (input.available() < 0) {
							System.out.print("[" + DateUtil.showTime() + "]"
									+ "Meter socket error ");
							break;
						}
					}
					/* check the rebootSignal */
					if(rebootSignal == 1){
						rebootREQ = new RebootMeterConfig_REQ(output);
						rebootREQ.start();
						output.flush();
						Thread.sleep(1000);
						if (input.available() > 0 && input.available() > 5){
							System.out.println("GET Reboot Command Response.");
							System.out.println(input.toString());
						}
						rebootSignal = 0;
						break;
					}

				} else {
					Thread.sleep(pollingTime);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("Thread STOP !!");
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			System.out.println("要結束了~!!!");
			try {
				//appREQ.stop();
				engerREQ.stop();
				sigREQ.stop();
				clientSocket.close();
				output.close();
				input.close();
				if (!conn.isClosed() && conn != null) {
					conn.close();
				}
				/*
				 * if(!conn2.isClosed()){ conn2.close(); }
				 */
				LogFile.info("------------------/資料庫連線關閉/----------------");

			} catch (SQLException e1) {
				e1.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}
	
	private int getSubtractSecond(String timestamp, String _insertTimeRecord) {
		if(_insertTimeRecord.isEmpty()) return 0;
		int r = 0;
		r += 10 * (Integer.parseInt(timestamp.substring(17, 18)) - Integer.parseInt(_insertTimeRecord.substring(17, 18)));
		r += (Integer.parseInt(timestamp.substring(18, 19)) - Integer.parseInt(_insertTimeRecord.substring(18, 19)));
		
		return r;
	}

	private byte num2byte(int phaseSetting) {
		switch (phaseSetting){
			case 1: return 0x01;
			case 2: return 0x02;
			case 4: return 0x04;
			case 7: return 0x07;
			case 10: return 0x10;
			case 17: return 0x17;
		}
		return 0;
	}

	public void setRebootSignal(int n){
			rebootSignal = n;
	}

	/**
	 * 將Appliance的資料存到Mysql SmartMeterDB.app_rec
	 * 
	 * @param cn
	 * @param info_in
	 * @param meterID
	 * @throws Exception
	 */
	public void insertAppliance(Connection cn, ApplianceInfo appInfo, int meterID) throws Exception {

		PreparedStatement stmt = null;
		// Connection xconn=null;
		// if(cn.isClosed() || cn==null)
		// {
		// cn=DBConnection.getConnection();
		//
		// }

		try {
			// xconn=DBConnection.getConnection();
			if (cn.isClosed() || cn == null) {
				cn = DBConnection.getConnection();
			}
			stmt = cn
					.prepareStatement("INSERT INTO APP_EventRecord(`timestamp`,`meterID`,`phaseID`, `appID`, `typeID`, `stateID`, `signatureIdx`,"
							+ " `voltage`, `current`, `power`, `energy`, `power_fac`, `cluster_num`,"
							+ " `cid1`, `cid2`, `cid3`, `cid4`, `cid5`, `cid6`, `cid7`, `cid8`, `cid9`, `cid10`,"//, `cid11`, `cid12`, `cid13`, `cid14`, `cid15`, `cid16`, `cid17`, `cid18`, `cid19`, `cid20`) "
							+ " `distance1`, `distance2`, `distance3`, `distance4`, `distance5`, `distance6`, `distance7`, `distance8`, `distance9`, `distance10`) "//, `distance11`, `distance12`, `distance13`, `distance14`, `distance15`, `distance16`, `distance17`, `distance18`, `distance19`, `distance20`) "
							+ " VALUES (?,?,?,?,?,?,?"
							+ ",?,?,?,?,?,?"
							+ ",?,?,?,?,?,?,?,?,?,?"//,?,?,?,?,?,?,?,?,?,?"
							+ ",?,?,?,?,?,?,?,?,?,?)");//,?,?,?,?,?,?,?,?,?,?)");
			
			stmt.setString(1, appInfo.getTimestamp());
			stmt.setInt(2, meterID);
			stmt.setInt(3, appInfo.getPhaseID());
			stmt.setInt(4, appInfo.getAppID());
			stmt.setInt(5, appInfo.getAppType());
			stmt.setInt(6, appInfo.getAppState());
			stmt.setInt(7, appInfo.getSignatureIndex());
			stmt.setFloat(8, appInfo.getVoltage());
			stmt.setFloat(9, appInfo.getCurrent());
			stmt.setFloat(10, appInfo.getPower());
			stmt.setFloat(11, appInfo.getEnergy());
			stmt.setFloat(12, appInfo.getFactor());
			stmt.setFloat(13, appInfo.getClusterNumber());
			int[] cids = new int[20];
			cids = appInfo.getCids();
			for(int i = 0; i<10; i++)
				stmt.setInt(14+i, cids[i]);
			float[] distances = new float[20];
			distances = appInfo.getDistances();
			for(int i = 0; i<10; i++)
				stmt.setFloat(24+i, distances[i]);
			stmt.executeUpdate();
			LogFile.debug(stmt.toString());

		} catch (Exception e) {
			LogFile.error(e.toString());
		} finally {
			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException e) {
				LogFile.error(e.toString());
			}
			// xconn.close();

		}
	}

	/**
	 * 把資料加入table:power
	 * 
	 * @param cn
	 * @param info_in
	 * @param meterID
	 * @throws Exception
	 */
	public void insertEnergy(Connection cn, EnergyInfo info, int meterID) {

		// Connection xconn=null;
		PreparedStatement stmt = null;
		try {
			// stmt.setQueryTimeout(60); // if can't inset 60s
			if (cn.isClosed() || cn == null) {
				cn = DBConnection.getConnection();

			}
			
			stmt = cn.prepareStatement("INSERT INTO Energy(`meterID`, `timestamp`, `phaseID`, `voltage`, `current`, `pwr_factor`, `Hz`, `active_pwr`, `reactive_pwr`, `apparence_pwr`, `kw_hr`, `var_hr`, `va_hr`,`fae`) "
							+ "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
			stmt.setInt(1, meterID);
			//stmt.setString(2, rounded(info.getTimestamp()));
			stmt.setString(2, info.getTimestamp());
			stmt.setInt(3, info.getPahaseId());
			stmt.setFloat(4, info.getV());
			stmt.setFloat(5, info.getA());// current
			stmt.setFloat(6, info.getFactor());// pwr_factor
			stmt.setFloat(7, info.getHz());// HZ
			stmt.setFloat(8, info.getW());// active_pwr
			stmt.setFloat(9, info.getVAR());// reactive_pwr
			stmt.setFloat(10, info.getVA());// apparence_pwr
			stmt.setFloat(11, info.getWh() / 1000);// kw_hr
			stmt.setFloat(12, info.getVARh());// var_hr
			stmt.setFloat(13, info.getVAh());// va_hr
			stmt.setFloat(14, 0);// fae
			LogFile.debug(stmt.toString());
			stmt.executeUpdate();

		} catch (Exception e) {
			System.out.println("in insert function:" +e.toString());
			LogFile.error(e.toString());
		} finally {
			try {
				if (stmt != null) stmt.close();
			} catch (SQLException e) {
				LogFile.error(e.toString());
			}

		}

	}
	
	public void insertSignature(Connection cn, SignatureInfo info, int meterID) {
		if(info.IsSnapshot == 1) return;
		// Connection xconn=null;
		PreparedStatement stmt = null;
		try {
			// stmt.setQueryTimeout(60); // if can't inset 60s
			if (cn.isClosed() || cn == null) {
				cn = DBConnection.getConnection();

			}
			
			stmt = cn.prepareStatement("INSERT INTO Signature(`signID`, `meterID`, `timestamp`, `snapshot`, `phaseID`, `vrms`, `irms`, `avgP`, `avgQ`, `maxP`, `maxQ`, "
					+ "`P`, `Q`, `har0`, `har1`, `har2`, `har3`, `har4`, `har5`, `har6`, `har7`, `har8`, `har9`)" 
					+ "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
			stmt.setInt(1, info.SignatureID);
			//stmt.setString(2, rounded(info.getTimestamp()));
			stmt.setInt(2, meterID);
			stmt.setString(3, info.getTimestamp());
			stmt.setFloat(4, info.IsSnapshot);
			stmt.setFloat(5, info.PhaseId); 	// phaseID 
			stmt.setFloat(6, info.getSteadyVrms());// steady Vrms
			stmt.setFloat(7, info.getSteadyIrms());// steady Irms 
			stmt.setFloat(8, info.getTransientAvgP());// transient average active_pwr
			stmt.setFloat(9, info.getTransientAvgQ());// transient average reactive_pwr
			stmt.setFloat(10, info.getTransientMaxP());
			stmt.setFloat(11, info.getTransientMaxQ());
			stmt.setFloat(12, info.getSteadyP());// 
			stmt.setFloat(13, info.getSteadyQ());// 
			float[] harmonics = new float[10];
			harmonics = info.getSteadyCurrentHarmonic();
			stmt.setFloat(14, harmonics[0]);// Harmonics
			stmt.setFloat(15, harmonics[1]);// Harmonics
			stmt.setFloat(16, harmonics[2]);// Harmonics
			stmt.setFloat(17, harmonics[3]);// Harmonics
			stmt.setFloat(18, harmonics[4]);// Harmonics
			stmt.setFloat(19, harmonics[5]);// Harmonics
			stmt.setFloat(20, harmonics[6]);// Harmonics
			stmt.setFloat(21, harmonics[7]);// Harmonics
			stmt.setFloat(22, harmonics[8]);// Harmonics
			stmt.setFloat(23, harmonics[9]);// Harmonics
			LogFile.debug(stmt.toString());
			stmt.executeUpdate();

		} catch (Exception e) {
			System.out.println("in insert function:" +e.toString());
			LogFile.error(e.toString());
		} finally {
			try {
				if (stmt != null) stmt.close();
			} catch (SQLException e) {
				LogFile.error(e.toString());
			}

		}

	}
	
	private String rounded(String timestamp) {
		String r_string = timestamp.substring(0, 17);
		if(Integer.parseInt(timestamp.substring(18, 19)) >= 5) r_string += Integer.toString(Integer.parseInt(timestamp.substring(17,18) + 1));
		else r_string += timestamp.substring(17, 18);
		r_string += "0";
		//return timestamp;
		return r_string;
	}

	/**
	 * 把資料加入table:Energy_15m
	 * 
	 * @param cn
	 * @param info_in
	 * @param meterID
	 * @throws Exception
	 */
	public void insertEnergy_15m(Connection cn, EnergyInfo info, int meterID, float energy, float avg_active_pwr) {
		// Connection xconn=null;
		PreparedStatement stmt = null;
		try {
			// stmt.setQueryTimeout(60); // if can't insert 60s
			if (cn.isClosed() || cn == null) {
				cn = DBConnection.getConnection();
			}
			stmt = cn.prepareStatement("INSERT INTO Energy_15m(`meterID`, `timestamp`, `phaseID`,  `kw_hr`, `avg_active_pwr`) "	+ "VALUES (?,?,?,?,?)");
			stmt.setInt(1, meterID);
			stmt.setString(2, rounded(info.getTimestamp()));
			stmt.setInt(3, info.getPahaseId());
			stmt.setFloat(4,  energy);
			stmt.setFloat(5,  avg_active_pwr);
			LogFile.debug(stmt.toString());
			if(forNCKU == 1)
				stmt.executeUpdate();

		} catch (Exception e) {
			LogFile.error(e.toString());
		} finally {
			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException e) {
				LogFile.error(e.toString());
			}

		}

	}

	/**
	 * 把資料加入table:Energy_1hr
	 * 
	 * @param cn
	 * @param info_in
	 * @param meterID
	 * @throws Exception
	 */
	public void insertEnergy_1hr(Connection cn, EnergyInfo info, int meterID, float energy) {
		// Connection xconn=null;
		PreparedStatement stmt = null;
		try {
			// stmt.setQueryTimeout(60); // if can't insert 60s
			if (cn.isClosed() || cn == null) {
				cn = DBConnection.getConnection();
			}
			stmt = cn
					//.prepareStatement("INSERT INTO Energy(`meterID`, `timestamp`, `phaseID`, `voltage`, `current`, `pwr_factor`, `Hz`, `active_pwr`, `reactive_pwr`, `apparence_pwr`, `kw_hr`, `var_hr`, `va_hr`,`fae`) "
					.prepareStatement("INSERT INTO Energy_1hr(`meterID`, `timestamp`, `phaseID`,  `kw_hr`) " + "VALUES (?,?,?,?)");
			stmt.setInt(1, meterID);
			stmt.setString(2, rounded(info.getTimestamp()));
			stmt.setInt(3, info.getPahaseId());
			stmt.setFloat(4,  energy);
			LogFile.debug(stmt.toString());
			if(forNCKU == 1)
				stmt.executeUpdate();

		} catch (Exception e) {
			LogFile.error(e.toString());
		} finally {
			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException e) {
				LogFile.error(e.toString());
			}

		}

	}
	
	public void insertEnergy_period(int type,Connection cn, EnergyInfo info, int meterID, float energy) { //type 1 = 1 day. type 2 = 1 month(30day)
		// Connection xconn=null;
		PreparedStatement stmt = null;
		try {
			// stmt.setQueryTimeout(60); // if can't insert 60s
			if (cn.isClosed() || cn == null) {
				cn = DBConnection.getConnection();
			}
			if(type == 1)
				stmt = cn.prepareStatement("INSERT INTO Energy_1day(`meterID`, `timestamp`, `phaseID`,  `kw_hr`) " + "VALUES (?,?,?,?)");
			else if (type == 2)
				stmt = cn.prepareStatement("INSERT INTO Energy_1month(`meterID`, `timestamp`, `phaseID`,  `kw_hr`) " + "VALUES (?,?,?,?)");
			stmt.setInt(1, meterID);
			stmt.setString(2, rounded(info.getTimestamp()));
			stmt.setInt(3, info.getPahaseId());
			stmt.setFloat(4,  energy);
			LogFile.debug(stmt.toString());
			if(forNCKU == 1)
				stmt.executeUpdate();

		} catch (Exception e) {
			LogFile.error(e.toString());
		} finally {
			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException e) {
				LogFile.error(e.toString());
			}

		}
	}
	
	public boolean checkTimestamp(int time_leng, String origin_timestamp){  // format: 2013-11-15 19:09:18      //NCKU need 15:00 minutes sharp
		if(origin_timestamp.charAt(17) != '0' || origin_timestamp.charAt(18) != '0')
			return false;
		else if(time_leng == 15){  // 15 min
			if(origin_timestamp.charAt(14) == '0' || origin_timestamp.charAt(14) == '3'){
				if(origin_timestamp.charAt(15) == '0'){
					return true;
				}
			}else if(origin_timestamp.charAt(14) == '1' || origin_timestamp.charAt(14) == '4'){
				if(origin_timestamp.charAt(15) == '5'){
					return true;
				}
			}
			return false;
		}else if(time_leng == 60){ // 1 hr
			if(origin_timestamp.charAt(14) == '0' && origin_timestamp.charAt(15) == '0' ){
				return true;
			}else 
				return false;
		}else if(time_leng == 1440){ // 1 day
			if(origin_timestamp.charAt(11) == '0' && origin_timestamp.charAt(12) == '0' && origin_timestamp.charAt(14) == '0' && origin_timestamp.charAt(15) == '0'){
				return true;
			}else 
				return false;
		}
		return false;
	}
}
