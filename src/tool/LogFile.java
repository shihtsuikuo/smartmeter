/**
 * @author : Administrator
 * @date : 2012/5/21
 * @vision :
 * @E-Mail : sky12999@gmail.com
 * @Goal :設定log file
 */

package tool;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

public class LogFile {
	private static Logger log;

	public void setPropertyConfig(String property) {
		PropertyConfigurator.configure(property);
	}

	public LogFile(String property, String proName, Level setLevel) {
		this.setPropertyConfig(property);
		log = Logger.getLogger(proName);
		log.setLevel(setLevel);
		// this.setLevel(setLevel);
	}

	public static void main(String[] args){
		Test t = new Test();
		t.show();
		t.test();
	}

	public void trace(Object message) {
		log.trace(message);
	}

	public void error(Object message) {
		log.error(message);
	}

	public void debug(Object message) {
		log.debug(message);
	}

	public void info(Object message) {
		log.info(message);
	}

	public void fatal(Object message) {
		log.fatal(message);
	}
}
