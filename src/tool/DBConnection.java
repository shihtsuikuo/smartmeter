/**
 * @author : Leo, Kuo
 * @date : 2012/5/20
 * @vision :1.0.0
 * @E-Mail : sky12999@gmail.com
 * @Goal : 查詢初始化資料厙狀態
 */

package tool;

import java.util.Properties;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.sql.*;

public class DBConnection {
	private static Properties props;

	/**
	 * 讀取properties
	 */
	private static void loadProperties() {
		props = new Properties();
		try {
			props.load(new FileInputStream("DBconfig.properties"));
		} catch (FileNotFoundException e) {
			System.out.println("Can't find file.");
		} catch (IOException e) {
			System.out.println(e.toString());
		}
	}

	private static String getConfig(String key) {
		return props.getProperty(key);
	}

	public static void main(String[] args) {
		
		test();
		
		
		
		
	}

	public static void test() {
		loadProperties();
		String driver = getConfig("MySql_driver");
		String url = getConfig("MySql_url");
		String user = getConfig("MySql_user");
		String password = getConfig("MySql_password");

		try {
			Class.forName(driver);
			Connection conn = DriverManager.getConnection(url, user, password);

			if (conn != null && !conn.isClosed()) {

				System.out.println("資料庫連線測試成功！");
				System.out
						.println("--------JDBC Connection \t----------------------");
				System.out.println("--------url:" + url
						+ "\t----------------------");
				System.out.println("--------username:" + user
						+ "\t----------------------");
				System.out.println("--------password:" + password
						+ "\t----------------------");
				conn.close();
			}

		} catch (ClassNotFoundException e) {
			System.out.println("找不到驅動程式類別");
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 設定JDBC connection 連線
	 * 
	 * @return
	 * @throws Exception
	 */
	public static Connection getConnection() throws Exception {
		// Load the JDBC driver
		loadProperties();
		String driver = getConfig("MySql_driver");
		Class.forName(driver);
		String url = getConfig("MySql_url");
		String username = getConfig("MySql_user");
		String password = getConfig("MySql_password");
		// LogFile.println("--------JDBC Connection \t----------------------");
		// LogFile.println("--------url:"+url+"\t----------------------");
		// LogFile.println("--------username:"+username+"\t----------------------");
		// LogFile.println("--------password:"+password+"\t----------------------");
		return DriverManager.getConnection(url, username, password);
	}

	/*
	 * public static Connection getConnection2() throws Exception { // Load the
	 * JDBC driver loadProperties(); String driver = getConfig("MySql_driver");
	 * Class.forName(driver); String url = getConfig("MySql_url_2"); String
	 * username = getConfig("MySql_user"); String password =
	 * getConfig("MySql_password"); //
	 * LogFile.println("--------JDBC Connection \t----------------------"); //
	 * LogFile.println("--------url:"+url+"\t----------------------"); //
	 * LogFile.println("--------username:"+username+"\t----------------------");
	 * //
	 * LogFile.println("--------password:"+password+"\t----------------------");
	 * return DriverManager.getConnection(url, username, password); }
	 */

}