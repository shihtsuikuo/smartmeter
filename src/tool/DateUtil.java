/**
 * @author : Loe
 * @date : 2012/5/30
 * @vision : 1.0.0
 * @E-Mail : sky12999@gmail.com
 * @Goal :常用日期工具
 */

package tool;

import java.text.DateFormat;
import java.util.Date;

public class DateUtil {
	/**
	 * 取得系統的時間，單位millisecond
	 * 
	 * @return
	 */
	public static long showTimeLong() {
		long output = 0;
		Date date = new Date();
		output = date.getTime();
		return output;
	}

	/**
	 * 取得系統時間，Type :YYYY /MM /DD hh:mm:ss
	 * 
	 * @return
	 */
	public static String showTime() {

		Date date = new Date();
		DateFormat mediumFormat = DateFormat.getDateTimeInstance(
				DateFormat.MEDIUM, DateFormat.MEDIUM);
		return "" + mediumFormat.format(date);
	}
}
