/**
 * @author : Administrator
 * @date : 2012/5/31
 * @vision :
 * @E-Mail : sky12999@gmail.com
 * @Goal :
 * @memo : #212 Frequency 暫時設成INT
 */
package bean;

import java.math.BigInteger;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.driver.Protocol;

import tool.LogFile;

public class SignatureInfo {
	public int SignatureID;
	public int IsSnapshot;
	public int PhaseId;
	public String Timestamp;
	public long timestampLong;
	public float SteadyVrms;
	public float SteadyIrms;
	public float TransientAvgP;
	public float TransientAvgQ;
	public float TransientMaxP;
	public float TransientMaxQ;
	public float SteadyP;
	public float SteadyQ;
	public float[] SteadyCurrentHarmonic = new float[10];

	public int getSignatureId() {
		return SignatureID;
	}

	public void setSignatureID(int sigid) {
		SignatureID = sigid;
	}

	public String getTimestamp() {
		return Timestamp;
	}

	public void setTimestamp(String timestamp) {
		Timestamp = timestamp;
	}

	public int getPhaseId() {
		return PhaseId;
	}

	public void setPhaseId(int phaseId) {
		PhaseId = phaseId;
	}

	public int getIsSnapshot() {
		return IsSnapshot;
	}
	
	public void setIsSnapshot(int isSnapshopt) {
		IsSnapshot = isSnapshopt;
	}

	public float getSteadyVrms() {
		return SteadyVrms;
	}

	public void setSteadyVrms(float v) {
		SteadyVrms = v;
	}

	public float getSteadyIrms() {
		return SteadyIrms;
	}

	public void setSteadyIrms(float irms) {
		SteadyIrms = irms;
	}

	public float getTransientAvgP() {
		return TransientAvgP;
	}

	public void setTransientAvgP(float TavgP) {
		TransientAvgP = TavgP;
	}

	public float getTransientAvgQ() {
		return TransientAvgQ;
	}

	public void setTransientAvgQ(float TavgQ) {
		this.TransientAvgQ = TavgQ;
	}

	public float getTransientMaxP() {
		return TransientMaxP;
	}

	public void setTransientMaxP(float TmaxP) {
		TransientMaxP = TmaxP;
	}

	public float getTransientMaxQ() {
		return TransientMaxQ;
	}

	public void setTransientMaxQ(float TmaxQ) {
		TransientMaxQ = TmaxQ;
	}

	public float getSteadyP() {
		return SteadyP;
	}

	public void setSteadyP(float sP) {
		SteadyP = sP;
	}

	public float getSteadyQ() {
		return SteadyQ;
	}

	public void setSteadyQ(float sQ) {
		SteadyQ = sQ;
	}

	public float[] getSteadyCurrentHarmonic() {
		return SteadyCurrentHarmonic;
	}

	public void setSteadyCurrentHarmonic(int index, float sCurrentHarm) {
		SteadyCurrentHarmonic[index] = sCurrentHarm;
	}

	public long getTimestampLong() {
		return timestampLong;
	}

	public void setTimestampLong(long timestampLong) {
		this.timestampLong = timestampLong;
	}
	/**
	 * 
	 * @param data
	 * @return
	 */
	public SignatureInfo getSig(byte[] data) {
		
		SignatureInfo info_in = new SignatureInfo();
		byte[] sigid = {data[2],data[3],data[4],data[5]};
		info_in.setSignatureID(Protocol.getint(sigid, 0));
		info_in.setIsSnapshot(Integer.parseInt(Byte.toString(data[6])));// snapshot
		info_in.setPhaseId(Integer.parseInt(Byte.toString(data[7]))); // phaseID
		byte[] time = {data[8],data[9],data[10],data[11]};
		info_in.setTimestamp(Protocol.byteToTimestampString(time)); 
		info_in.setTimestampLong(Protocol.byteToTimestampLong(time));
	
		info_in.setSteadyVrms(Protocol.getFloat(data, 12));// Steady Vrms
		info_in.setSteadyIrms(Protocol.getFloat(data, 16));// Steady Irms
		info_in.setTransientAvgP(Protocol.getFloat(data, 20));// Transient Average Active Power
		info_in.setTransientAvgQ(Protocol.getFloat(data, 24));// Transient Average Reactive Power
		info_in.setTransientMaxP(Protocol.getFloat(data, 28));// Transient Maximum Active Power
		info_in.setTransientMaxQ(Protocol.getFloat(data, 32));// Transient Maximum Reactive Power
		info_in.setSteadyP(Protocol.getFloat(data, 36));// Steady Active Power
		info_in.setSteadyQ(Protocol.getFloat(data, 40));// Steady Reactive Power
		info_in.setSteadyCurrentHarmonic(0, Protocol.getFloat(data, 44));// Steady Current Harmonic
		info_in.setSteadyCurrentHarmonic(1, Protocol.getFloat(data, 48));// Steady Current Harmonic
		info_in.setSteadyCurrentHarmonic(2, Protocol.getFloat(data, 52));// Steady Current Harmonic
		info_in.setSteadyCurrentHarmonic(3, Protocol.getFloat(data, 56));// Steady Current Harmonic
		info_in.setSteadyCurrentHarmonic(4, Protocol.getFloat(data, 60));// Steady Current Harmonic
		info_in.setSteadyCurrentHarmonic(5, Protocol.getFloat(data, 64));// Steady Current Harmonic
		info_in.setSteadyCurrentHarmonic(6, Protocol.getFloat(data, 68));// Steady Current Harmonic
		info_in.setSteadyCurrentHarmonic(7, Protocol.getFloat(data, 72));// Steady Current Harmonic
		info_in.setSteadyCurrentHarmonic(8, Protocol.getFloat(data, 76));// Steady Current Harmonic
		info_in.setSteadyCurrentHarmonic(9, Protocol.getFloat(data, 80));// Steady Current Harmonic

		return info_in;
	}
	public void showSignature(SignatureInfo sig){
		
	}
}
