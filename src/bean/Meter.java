/**
 * @author : Administrator
 * @date : 2012/5/27
 * @vision :
 * @E-Mail : sky12999@gmail.com
 * @Goal : 建立 Mterer Bean 
 */


package bean;

	public class Meter{
	private String name;
	private String registerTime;
	private String hardwareAddr;
	private String model;
	private int phaseID;
	private int firmwareVersion;
	private int parentMeter;
	private String status;
	private int live;
	private int meterId;
	public int getMeterId() {
		return meterId;
	}
	public void setMeterId(int meterId) {
		this.meterId = meterId;
	}
	public String getName(){
		return this.name;
	}
	public void setName(String name){
		this.name=name;
	}
	public String getRegisterTime(){
		return this.registerTime;
	}
	public void setRegisterTime(String registerTime){
		this.registerTime=registerTime;
	}

	public String getHardwareAddr(){
		return this.hardwareAddr;
	}
	public void setHardwareAddr(String hardwareAddr){
		this.hardwareAddr=hardwareAddr;
	}
	public String getModel(){
		return this.model;
	}
	public void setModel(String model){
		this.model=model;
	}
	public int getPhaseID(){
		return this.phaseID;
	}
	public void setPhaseID(int phaseID){
		this.phaseID=phaseID;
	}
	public int getFirmwareVersion(){
		return this.firmwareVersion;
	}
	public void setFirmwareVersion(int firmwareVersion){
		this.firmwareVersion=firmwareVersion;
	}
	public int getParentMeter(){
		return this.parentMeter;
	}
	public void setParentMeter(int parentMeter){
		this.parentMeter=parentMeter;
	}
	public String getStatus(){
		return this.status;
	}
	public void setStatus(String status){
		this.status=status;
	}
	public int getLive(){
		return this.live;
	}
	public void setLive(int live){
		this.live=live;
	}

}