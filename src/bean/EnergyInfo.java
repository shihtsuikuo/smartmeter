/**
 * @author : Administrator
 * @date : 2012/5/31
 * @vision :
 * @E-Mail : sky12999@gmail.com
 * @Goal :
 * @memo : #212 Frequency 暫時設成INT
 */
package bean;

import java.math.BigInteger;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.driver.Protocol;

import tool.LogFile;

public class EnergyInfo {
	public String Timestamp;
	public long timestampLong;
	public int PahaseId;
	public float A;
	public float V;
	public float W;
	public float Wh;
	public float factor;
	public float Hz;
	public float VAR;
	public float VARh;
	public float VA;
	public float VAh;
	public float FAE;
	public float FOR5;

	public String getTimestamp() {
		return Timestamp;
	}

	public void setTimestamp(String timestamp) {
		Timestamp = timestamp;
	}

	public int getPahaseId() {
		return PahaseId;
	}

	public void setPahaseId(int pahaseId) {
		PahaseId = pahaseId;
	}

	public float getA() {
		return A;
	}

	public void setA(float a) {
		A = a;
	}

	public float getV() {
		return V;
	}

	public void setV(float v) {
		V = v;
	}

	public float getW() {
		return W;
	}

	public void setW(float w) {
		W = w;
	}

	public float getWh() {
		return Wh;
	}

	public void setWh(float wh) {
		Wh = wh;
	}

	public float getFactor() {
		return factor;
	}

	public void setFactor(float factor) {
		this.factor = factor;
	}

	public float getHz() {
		return Hz;
	}

	public void setHz(float hz) {
		Hz = hz;
	}

	public float getVAR() {
		return VAR;
	}

	public void setVAR(float vAR) {
		VAR = vAR;
	}

	public float getVARh() {
		return VARh;
	}

	public void setVARh(float vARh) {
		VARh = vARh;
	}

	public float getVA() {
		return VA;
	}

	public void setVA(float vA) {
		VA = vA;
	}

	public float getVAh() {
		return VAh;
	}

	public void setVAh(float vAh) {
		VAh = vAh;
	}

	public float getFAE() {
		return FAE;
	}

	public void setFAE(float fAE) {
		FAE = fAE;
	}

	public float getFOR5() {
		return FOR5;
	}

	public void setFOR5(float fOR5) {
		FOR5 = fOR5;
	}

	public long getTimestampLong() {
		return timestampLong;
	}

	public void setTimestampLong(long timestampLong) {
		this.timestampLong = timestampLong;
	}
	/**
	 * 
	 * @param data
	 * @return
	 */
	public EnergyInfo getEnergy(byte[] data) {
		EnergyInfo info_in = new EnergyInfo();
		// Phase ID
		// Meter 之 timestamp 有待修正
		 //LogFile.print("test1="+Integer.toBinaryString(Byte.parseByte(""+data[1])));
		 //LogFile.print(""+Integer.toBinaryString(Byte.parseByte(""+data[5])));
		 //LogFile.print(","+Integer.toBinaryString(Byte.parseByte(""+data[4])));
		 //LogFile.print(","+Integer.toBinaryString(Byte.parseByte(""+data[3])));
		 //LogFile.println(","+Integer.toBinaryString(Byte.parseByte(""+data[2])));
		 
		 //LogFile.println("Today = "+Long.toBinaryString(date.getTime()));
		 //LogFile.println("test2="+Integer.toBinaryString(Byte.parseByte(""+data[6])));
		 //LogFile.println(Protocal.getFloat(data, 2));
		 //LogFile.println("PahaseId >>> "+Protocal.getint(data, 6));
		
		// A
		
		//info_in.PahaseId = Integer.parseInt(Byte.toString(data[6])); // phase
		 
	


		 byte[] time = {data[2],data[3],data[4],data[5]};
		info_in.setTimestamp(Protocol.byteToTimestampString(time)); 
		info_in.setTimestampLong(Protocol.byteToTimestampLong(time));
		info_in.setPahaseId(Integer.parseInt(Byte.toString(data[6]))); // phase id
		info_in.setA(Protocol.getFloat(data, 7));// 電流
		
		//LogFile.println("-------------------------------------------------------");
		//LogFile.println(Integer.toHexString(Integer.parseInt(""+Byte.toString((data[10])))));
		//LogFile.println(Integer.toHexString(Integer.parseInt(""+Byte.toString((data[9])))));
		//LogFile.println(Integer.toHexString(Integer.parseInt(""+Byte.toString((data[8])))));
		//LogFile.println(Integer.toHexString(Integer.parseInt(""+Byte.toString((data[7])))));
		//LogFile.println("-------------------------------------------------------");
		info_in.setV(Protocol.getFloat(data, 11));// 電壓
		//LogFile.println(Integer.toHexString(Integer.parseInt(""+Byte.toString((data[14])))));
		//LogFile.println(Integer.toHexString(Integer.parseInt(""+Byte.toString((data[13])))));
		//LogFile.println(Integer.toHexString(Integer.parseInt(""+Byte.toString((data[12])))));
		//LogFile.println(Integer.toHexString(Integer.parseInt(""+Byte.toString((data[11])))));
		info_in.setW(Protocol.getFloat(data, 15));// Active Power
		//LogFile.println("-------------------------------------------------------");
		//LogFile.println(Integer.toHexString(Integer.parseInt(""+Byte.toString((data[18])))));
		//LogFile.println(Integer.toHexString(Integer.parseInt(""+Byte.toString((data[17])))));
		//LogFile.println(Integer.toHexString(Integer.parseInt(""+Byte.toString((data[16])))));
		//LogFile.println(Integer.toHexString(Integer.parseInt(""+Byte.toString((data[15])))));
		//LogFile.println("-------------------------------------------------------");
		info_in.setWh(Protocol.getFloat(data, 19));// Active Energy
		info_in.setFactor(Protocol.getFloat(data, 23));// Power factor
		info_in.setHz(Protocol.getFloat(data, 27));// Frequency  // 暫時設成INT
		info_in.setVAR(Protocol.getFloat(data, 31));// Reactive Power
		info_in.setVARh(Protocol.getFloat(data, 35));// Reactive Energy
		info_in.setVA(Protocol.getFloat(data, 39));// Appearance Power
		info_in.setVAh(Protocol.getFloat(data, 43));// Appearance Energy
		info_in.setFAE(Protocol.getFloat(data, 47));// Fundamental active energy
		//info_in.setFOR5(Protocal.getFloat(data, 51));// For 0x05
		return info_in;
	}
	public void showEnergy(EnergyInfo energy){
		
	}
}
