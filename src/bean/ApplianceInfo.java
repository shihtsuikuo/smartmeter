package bean;

import java.sql.SQLException;
import java.util.Arrays;

import org.apache.log4j.*;

import com.driver.Protocol;

public class ApplianceInfo {
	/* Meter firmware appInfo structure 
	 * typedef struct __attribute__((__packed__))
	{
		uint16_t cmd;
		uint32_t timestamp;
		uint8_t  phaseID; 
		uint32_t appID;
		uint32_t typeID;
		uint32_t stateID;
		float voltage;
		float current;
		float active_power;
		float active_energy;
		float PF;
		uint32_t sigIndex;
		uint8_t  event_type;
	
		uint32_t	cluster_num;
		int like_cid[20];
		float like_value[20];
	} APP_UpdateInfo;   
    */
	public int meterid;
	public String timestamp;
	public int phaseID;
	public int appID;
	public int appType;
	public int appState;
	public float voltage;
	public float current;
	public float power;
	public float energy;
	public float factor;
	public int SignatureIndex ;
	public long timestamp_long;  //seems it is not used in new version
	public int clusterNumber;
	public int[] cids = new int[20];
	public float[] distances = new float[20];
	
	private static Logger logAPP;
	static{
		PropertyConfigurator.configure("appliance.properties");
		logAPP = Logger.getLogger(ApplianceInfo.class);
		logAPP.info("------Appliance Event--------");
		//logAPP.error("error");
		//logAPP.warn("warn");
		//logAPP.debug("debug");
	}
	public float getEnergy() {
		return energy;
	}

	public void setEnergy(float energy) {
		this.energy = energy;
	}

	public long getTimestamp_long() {
		return timestamp_long;
	}

	public void setTimestamp_long(long timestamp_long) {
		this.timestamp_long = timestamp_long;
	}

	public int getMeterid() {
		return meterid;
	}

	public void setMeterid(int meterid) {
		this.meterid = meterid;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}
	
	public int getPhaseID() {
		return phaseID;
	}

	public void setPhaseID(int pID) {
		this.phaseID = pID;
	}
	
	public int getAppID() {
		return appID;
	}

	public void setAppID(int appID) {
		this.appID = appID;
	}

	public int getAppType() {
		return appType;
	}

	public void setAppType(int appType) {
		this.appType = appType;
	}

	public int getAppState() {
		return appState;
	}

	public void setAppState(int appState) {
		this.appState = appState;
	}
	public float getVoltage() {
		return voltage;
	}

	public void setVoltage(float v) {
		this.voltage = v;
	}
	public float getCurrent() {
		return current;
	}

	public void setCurrent(float c) {
		this.current = c;
	}

	public float getPower() {
		return power;
	}

	public void setPower(float power) {
		this.power = power;
	}

	public float getFactor() {
		return factor;
	}

	public void setFactor(float factor) {
		this.factor = factor;
	}
	
	public int getSignatureIndex() {
		return SignatureIndex;
	}

	public void setSignatureIndex(int signatureIndex) {
		SignatureIndex = signatureIndex;
	}

	public int getClusterNumber() {
		return clusterNumber;
	}

	public void setClusterNumber(int cNum) {
		clusterNumber = cNum;
	}
	
	public int[] getCids() {
		return cids;
	}

	public void setCids(int index, int c) {
		cids[index] = c;
	}
	
	public float[] getDistances() {
		return distances;
	}

	public void setDistances(int index, float dist) {
		distances[index] = dist;
	}
	/**
	 * package decoding for Appliance from
	 * @param input
	 * @return
	 */
	public ApplianceInfo getAppInfo(byte[] data){
		/*---- Meter firmware appInfo structure -----
		 * typedef struct __attribute__((__packed__))
		{
			uint16_t cmd;
			uint32_t timestamp;
			uint8_t  phaseID; 
			uint32_t appID;
			uint32_t typeID;
			uint32_t stateID;
			float voltage;
			float current;
			float active_power;
			float active_energy;
			float PF;
			uint32_t sigIndex;
			uint8_t  event_type;
		
			uint32_t	cluster_num;
			int like_cid[20];
			float like_value[20];
		} APP_UpdateInfo;   
	    */
		
		byte CRC  = Protocol.getCRC(data);
		ApplianceInfo info = new ApplianceInfo();
		String test = new String();
		 byte[] time = {data[2],data[3],data[4],data[5]};	
		 info.setTimestamp(Protocol.byteToTimestampString(time)); 
		 info.setTimestamp_long(Protocol.byteToTimestampLong(time));
		 info.setPhaseID(Integer.parseInt(Byte.toString(data[6])));
		 //System.out.println("PhaseID in hex:" + Arrays.toString(new byte[]{data[6]}));
		 info.setAppID(Protocol.getint(data, 7));
		 //System.out.println("AppID in hex:" + Arrays.toString(new byte[]{data[7],data[8],data[9],data[10]}));
		 info.setAppType(Protocol.getint(data, 11));
		 info.setAppState(Protocol.getint(data, 15));
		 info.setVoltage(Protocol.getFloat(data, 19));
		 info.setCurrent(Protocol.getFloat(data, 23));
		 //System.out.println("Current in hex:" + Arrays.toString(new byte[]{data[23],data[24],data[25],data[26]}));
		 info.setPower(Protocol.getFloat(data, 27));
		 //System.out.println("Power in hex:" + Arrays.toString(new byte[]{data[27],data[28],data[29],data[30]}));
		 info.setEnergy(Protocol.getFloat(data, 31));
		 info.setFactor(Protocol.getFloat(data, 35));
		 info.setSignatureIndex(Protocol.getint(data, 39));
		 //info.setEventType(Protocol.getint(data, 43));
		 info.setClusterNumber(Protocol.getint(data, 44));
		 //System.out.println("cluster number = " + Protocol.getint(data, 44));
		 for(int i = 0; i<10; i++)
			 info.setCids(i, Protocol.getint(data, 48+(4*i) ));
		 for(int i = 0; i<10; i++)
			 info.setDistances(i, Protocol.getFloat(data, 128+(4*i) ));
		 if(CRC != data[data.length-1]){
			 for (int i = 0; i < data.length; i++) {
				 System.out.print(data[i]+",");
				 test +=data[i]+",";
			}
			 logAPP.error("Applinace error"+test);
			 //System.out.println();
		 }
		return info;
	}
	public ApplianceInfo(){
		
	}
	public ApplianceInfo(java.sql.ResultSet rs,int appID,int meterID){
		try {
			this.setAppID(appID);
			this.setMeterid(meterID);
			this.setAppState(rs.getInt("status"));
				//System.out.println("get status");
			this.setPower(rs.getFloat("power"));
			//System.out.println("power is :"+rs.getFloat("power"));

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
	}
	public ApplianceInfo(int appID,int meterID,int state){
		this.setAppID(appID);
		this.setMeterid(meterID);
		this.setAppState(state);
	}
}
